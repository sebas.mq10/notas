# Episodio 10 - Open/Closed Principle

Todas las aplicaciones cambian durante su ciclo de vida, y siempre vendran nuevas versiones tras la primera release. No por ello debemos adelantarnos a desarrollar caracteristicas que el cliente podria necesitar en el futuro; si nos pusieramos en el papel de adivinos, seguramente fallariamos y probablemente desarrollariamos caracteristicas que el cliente nunca necesitara. El principio YAGNI ("you aint gonna need it"), utilizado en la Programacion Extrema, previene de implementar nada mas que lo que realmente se requiera. La idea es desarrollar ahora sobre los requisitos funcionales actuales, no sobre los que supongamos que apareceran dentro de un mes.
La actitud de adelantarnos a los acontecimientos es un mecanismo de defensa que en ocasiones acusamos los desarrolladores para prevenir lo que tarde o temprano sera inevitable: la modificacion. Lo unico que podemos hacer es minimizar el impacto de una futura modificacion en nuestro sistema, y para ello es imprescindible empezar con un buen disenio, ya que la modificacion de una clase o modulo de una aplicacion mal diseniada generara cambios en cascada sobre las clases dependientes que derivaran en unos efectos indeseables. La aplicacion se convierte, asi , en rigida, impredecible y no reutilizable.

### Principio Open/ Closed

*Una clase debe estar abierta a extensiones, pero cerrada a las modificaciones*.

OCP es la respuesta a la pregunta que haciamos anteriormente, ya que argumenta que deberiamos diseniar clases que nunca cambien, y que cuando un requisito cambie, lo que debemos hacer es extender el comportamiento de dichas clases aniadiendo codigo, no modificando el existente.

Las clases que cumplen con OCP tienen dos caracteristicas:

- Son abiertas para la extension, es decir, que la logica o el comportamiento de esas clases puede ser extendida en nuevas clases.
- Son cerradas para la modificacion, y por tanto el codigo fuente de dichas clases deberia permanecer inalterado.

