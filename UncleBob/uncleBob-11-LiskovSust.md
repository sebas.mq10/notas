# Episodio 11 - The Liskov Substitution Principle

El principio de sustitucion de Liskov o LSP es un principio de la programacion orientada a objetos, y puede definirse como: *Cada clase que hereda de otra puede usarse como su padre sin necesidad de conocer las diferencias entre ellas.*
Si **S** es un subtipo de **T**,entonces los objetos de tipo **T** en un programa de computadora pueden ser sustituidos por objetos de tipo **S** (es decir, los objetos de tipo **S** pueden sustituir objetos de tipo **T**), sin alterar ninguna de las propiedades deseables de ese programa (la correccion, la tarea que realiza, etc).
El LSP es una definicion particular de una relacion de subtipificacion, llamada tipificacion (fuerte) del comportamiento, que fue introducido inicialmente por Barbara Liskov en una conf.titulada *La Abstraccion de Datos y Jerarquia*. Se refiere mas a una relacion semantica que a una relacion sintactica, ya que solo tiene la intencion de garantizar la interoperabilidad semantica de tipos en una jerarquia, los tipos de objeto en particular. Liskov y Wing formularon el principio de la siguiente manera:

> *Sea ø(**x**) una propiedad comprobable acerca de los objetos **x** de tipo T. Entonces ø(**y**) debe ser verdad para los objetos **y** del topo **S**, donde **S** es un subtipo de **T**.*

----






