# Episodio 12 - The Interface segregation principle

Uncle Bob lo define como:

- * "Los clientes no deben ser obligados a depender de interfaces que no utilizan"*

Tambien se puede definir como la priorizacion de la creacion de multiples interfaces especificas, en lugar de una unica interfaz de uso general. Como resultado de su aplicacion,se facilita mantener una arquitectura desacoplada, resultando mas sencillo realizar refactorizaciones y modificaciones.
Para entender este principio lo primero es tener claro la funcionalidad de una interfaz dentro del diseño orientado a objetos. Las interfaces proporcionan una capa de abstraccion que nos ayudan a desacoplar modulos. De esta manera, la interfaz define el comportamiento que nuestro codigo espera para comunicarse con otros modulos, a traves de un conjunto de metodos y propiedades.
La conclusion obtenida de este principio es que **ninguna clase deberia depender de metodos que no usa**. Por tanto, cuando creemos interfaces que definen comportamientos, es importante estar seguros de que todas las clases que implementen esas interfaces vayan a necesitar y ser capaces de agregar comportamientos a todos los metodos. En caso contrario, es mejor tener varias interfaces mas pequeñas.
Como reflexion final, cuando un codigo depende de elementos o implementa metodos que no necesita, puede traer problemas no esperados.