Recopilar los requisitos escuchando al cliente, considerar diseniar la arquitectura en funcion de los casos de uso:
1. Enumerar casos de uso
2. Entidades de lista
3. Definir estructuras de datos (incluidas las reglas de negocio)
4. Definir actores/modulos para que se pueda particionar
- Es posible que se necesite un casting para no violar el principio de de sustitucion de Liskov.
- Demasiadas interfaces no son malas, no tienen codigo ejecutable.
- Un enfoque para romper las dependencias entre modulos, ademas del uso de interfaces, es escribir constructores (es decir, solicitudes) y fabricas (es decir, casos de uso).
- Diagramas de clase:
1. Util para compartir el proceso de pensamiento con otras personas.
2. Son muy faciles de quedar obsoletos