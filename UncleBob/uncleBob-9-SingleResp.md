# Episodio 9 - The Single Responsibility Principle

El principio de responsabilidad única o SRP (siglas del inglés, Single Responsibility Principle) en ingeniería de software establece que cada módulo o clase debe tener responsabilidad sobre una sola parte de la funcionalidad proporcionada por el software y esta responsabilidad debe estar encapsulada en su totalidad por la clase. Todos sus servicios deben estar estrechamente alineados con esa responsabilidad. Este principio está incluido en el acrónimo mnemotécnico SOLID. Robert C. Martin expresa el principio de la siguiente forma:

Una clase debe tener solo una razón para cambiar.

En programación orientada a objetos, se suele definir como principio de diseño que cada clase debe tener una única responsabilidad, y que esta debe estar contenida únicamente en la clase. Así:

- Una clase debería tener solo una razón para cambiar

- Cada responsabilidad es el eje del cambio

- Para contener la propagación del cambio, debemos separar las responsabilidades.

- Si una clase asume más de una responsabilidad, será más sensible al cambio.

-  Si una clase asume más de una responsabilidad, las responsabilidades se acoplan.