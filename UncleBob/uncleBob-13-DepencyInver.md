# Episodio 13 - The Dependency inversion principle

La definicion que da Uncle Bob originalmente es:

1. Los modulos de alto nivel no deberian depender de los modulos de bajo nivel. Ambos deberian depender de abstracciones.
2. Las abstracciones no deberian depender de los detalles. Los detalles deben depender de abstracciones.

- **Modulos de alto nivel**: Se refieren a los objetos que definen el que es y que hace tu programa. Aquellos que contienen la logica de negocio y como interactua el software entre si. **Son los objetos mas importantes del programa.** Por ejemplo, en una aplicacion de un banco, podria ser el objeto que se encarga de devolver la lista de cuentas bancarias.
- **Modulos de bajo nivel**: Son aquellos objetos que no estan directamente relacionados con la logica de negocio del programa. Por ejemplo,el mecanismo de persistencia (*CoreData,Realm,MySQL, etc..*) o el mecanismo de acceso a red (*URLSession,Alamofire,AFNetworking, etc..*). Son objetos menos importantes, de los cuales no depende la logica de negocio.
- **Abstracciones**: Se refieren a Tipos de Datos que no son las implementaciones concretas, si no los que definen la interfaz publica. Seran, por tanto, protocolos (o interfaces) o clases abstractas (* en Swift no existen las clases abstractas como tal).
- **Detalles**: Son implementaciones concretas, que contienen detalles de implementacion tales como:
1. Que mecanismo de persistencia se utiliza (*CoreData o Realm*)  - Objetos (*CoreDatabase* o *RealmDatabase*),o 
2. Que servicio se utiliza para acceder a la red (*Alamofire o URLSession*) - Objetos (*AlamofireWebService o URLSessionWebService*)

Los problemas que pueden resultar de no cumplir con el Principio de Inversion de Dependencias son:

- Aniades **Acoplamiento** entre modulos de alto nivel hacia modulos de bajo nivel. O lo que es lo mismo, tu logica de negocio dependera de detalles de implementacion. Si alguna vez quieres cambiar algun detalle de implementacion, por ejemplo utilizar *URLSession* o *Alamofire*,tendras que modificar tambien los objetos mas importantes del programa.
- **Las dependencias entre objetos no quedan claras**: a simple vista, no puedes saber facilmente de que otros objetos depende un modulo.
- **Tu programa no es testeable**: Si tu modulo depende de un objeto que no puedes intercambiar por un *mock* en tus tests no puedes testarla de manera unitaria. Si el test falla, no sabras realmente que objeto es el culpable del error.

### Que significa <<Inversion de Dependencias?>>

En este contexto, podemos entender la palabra "Inversion" por **cambiabilidad.**

**Ser capaz de "invertir" una dependencia es lo mismo que ser capaz de "intercambiar" una implementacion concreta por otra implementacion concreta cualquiera**.
Si tu puedes intercambiar una implementacion concreta por otra y tu programa sigue funcionando perfectamente, entonces estaras cumpliendo con el *Dependency inversion Principle*.

**No es lo mismo inversion de dependencias que inyeccion de dependencias.**

### Como saber si estamos violando el Dependency Inversion Principle?

**Siempre que instancies un objeto complejo en un otro objeto que lo necesita se estara violando el Dependency Inversion Principle.** Es decir, cuando uses el metodo *init* (o *new* en otros lenguajes de programacion), parate a pensar un par de minutos si tiene sentido inyectarlo en lugar de instanciarlo directamente.
Si no lo tiene, mueve la responsabilidad de construccion fuera del objeto e inyectalo en lugar de instanciarlo dentro. A la hora de inyectar las dependencias, fijate si estas inyectando la abstraccion o la implementacion concreta. ** Para que las dependencias sean invertibles (o cambiables/intercambiables) debes inyectar un objeto de tipo protocolo, y no la clase que lo implementa directamente.

### **Conclusion**

Este principio es uno de los mas importantes si quieres que tu codigo sea testable, legible y mantenible.

En resumen, cumplir con el DIP hace que el codigo de la logica de negocio de tu aplicacion no dependa de los detalles de implementacion, si no de abstracciones. Esto hara que en el futuro puedas cambiar los detalles de implementacion sin tener que modificar el codigo de tu logica de negocio.