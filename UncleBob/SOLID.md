# Foundations of the SOLID principles

Si hablamos de disenio y desarrollo de aplicaciones, **Principios SOLID** son unas palabras que debes conocer como uno de los fundamentos de la arquitectura y desarrollo de software. **SOLID** es el acronimo que acunio Michael Feathers, basandose en los principios de la *programacion orientada a objetos* que Robert C. Martin (*Uncle Bob*), habia recopilado en el anio 2000 en su paper *"Design Principles and Desing Patterns"*.

Los **5 principios SOLID** de disenio de aplicaciones de software son:

- S - Single Responsibility Principle (SRP)
- O - Open/Closed Principle (OCP)
- L - Liskov Substitution Principle (LSP)
- I - Interface Segregation Principle (ISP)
- D - Dependency Inversion Principle (DIP)

Entre los objetivos de tener en cuenta estos 5 principios a la hora de escribir codigo encontramos:

- Crear un **software eficaz** que cumpla con su cometido y que sea **robusto y estable**
- Escribir un **codigo limpio y flexible** ante los cambios:que se pueda modificar facilmente segun necesidad, que sea **reutilizable y mantenible**.
- Permitir **escalabilidad**: que acepte ser ampliado con nuevas funcionalidades de manera agil.

En definitiva, desarrollar un **software de calidad**.

En este sentido la aplicacion de los principios SOLID esta muy relacionada con la comprension y el uso de *patrones de disenio*, que nos permitiran mantener una **alta cohesion** y, por lo tanto, un **bajo acoplamiento** de software.

### Acoplamiento

 El acoplamiento se refiere al **grado de interdependencia que tienen dos unidades de software entre si**, entendiendo por unidades de software: clases, subtipos, metodos,modulos, funciones,bibliotecas, etc. Si dos unidades de software son completamente independientes la una de la otra, decimos que estan desacopladas.

 ### Cohesion

 La cohesion de software es el **grado en que los elementos diferentes de un sistema permanecen unidos para alcanzar un mejor resultado** que si trabajaran por separado. Se refiere a la forma en que podemos agrupar diversas unidades de software para crear una unidad mayor.


## 1. Principio de Responsabilidad Unica

Segun este principio "una clase deberia tener **una,y solo una,razon para cambiar"**.Es esto,precisamente, "razon para cambiar", lo que el Uncle Bob identifica como "responsabilidad".
El principio de Responsabilidad Unica es **el mas importante y fundamental de SOLID**, muy sencillo de explicar, pero el mas dificil de seguir en la practica.
El propio Bob resume como hacerlo: "Reune las cosas que cambian por las mismas razones. Separa aquellas que  cambian por razones diferentes".

## 2. Principio de Abierto/ Cerrado

"Deberias ser capaz de extender el comportamiento de una clase, sin modificarla".En otras palabras: las clases que usas deberian estar **abiertas para poder extenderse y cerradas para modificarse**.

## 3. Principio de Sustitucion de Liskov

**"Las clases derivadas deben poder sustituirse por sus clases base"**

Esto significa que los objetos deben poder ser reemplazados por instancias de sus subtipos sin alterar el correcto funcionamiento del sistema o lo que es lo mismo: si en un programa utilizamos cierta clase, **deberiamos poder usar cualquiera de sus subclases** sin interferir en la funcionalida del programa.

Segun el Uncle Bob incumplir este principio implica violar tambien el principio de Abierto/Cerrado.

## 4. Principio de Segregacion de la Interfaz

"Haz interfaces que sean especificas para un tipo de cliente", es decir, para una **finalidad concreta**
En este sentido, es preferible contar con muchas interfaces que definan pocos metodos que tener una interface forzada a implementar muchos metodos a los que no dara uso.

## 5. Principio de Inversion de Dependencias

**"Depende de abstracciones,** no de clases concretas". Uncle Bob recomienda:

1. Los modulos de alto nivel **no deberian depender de modulos de bajo nivel**. Ambos deberian depender de abstracciones.
2. **Las abstracciones no deberian depender de los detalles**. Los detalles deberian depender de las abstracciones.

El objetivo de este principio consiste en reducir las dependencias entre los modulos del codigo, es decir, alcanzar un bajo acoplamiento de las clases.
