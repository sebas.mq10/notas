git init
npx create-react-app@5.0.0 ./ --template typescript
npm start
git remote
git switch -c main (para cambiar a main si no esta, quizas no es necesario)
git push --set upstream origin main (o master)
npm i


# React

 ## Que es React?

 *  Es una libreria de Javascript declarativa,eficiente y flexible para construir interfaces. Permite componer interfaces de usuario complejas de piezas de codigo pequenias y aisladas llamadas "componentes".
 *  Un componente toma los parametros, llamados "props" (propiedades), y devuelve una jerarquia de vistas para mostrar a traves del metodo *render*.
 * El metodo *render* devuelve una descripcion de lo que queres ver en la pantalla. En particular, *render* devuelve un **React Element**,que es una descripcion ligera de que renderizar.La mayoria de los desarrolladores React usan una sintaxis especial llamada "JSX", por ejemplo: 
>const element = < h1 >Hello, world!</ h1 >

JSX produce "elementos" de React.

En el ejemplo a continuacion, declaramos una variable llamada *name* y luego la usamos dentro del JSX envolviendola dentro de llaves:

```
const name = 'Josh Perez';
const element = <h1>Hello, {name}</h1>;
```

En el ejemplo a continuacion, insertamos el resultado de llamar a la funcion de JS *formatName(user)*, dentro de un elemento < h1 >:

```
function formatName(user) {
  return user.firstName + ' ' + user.lastName;
}

const user = {
  firstName: 'Harper',
  lastName: 'Perez'
};

const element = (
  <h1>
    Hello, {formatName(user)}!
  </h1>
);
```
* Veamos este ejemplo:
```

        class ShoppingList extends React.Component {
            render() {
                return (
                    <div className="shopping-list">
                    <h1>Shopping List for {this.props.name}</h1>
                    <ul>
                        <li>Instagram</li>
                        <li>WhatsApp</li>
                        <li>Oculus</li>
                    </ul>
                    </div>
                );
            }
        }
```
* La sintaxis ```<div/>``` se transforma en el momento de la compilacion en React.createElement('div'). El ejemplo anterior es equivalente a:
```
        return React.createElement('div', {className: 'shopping-list'},
            React.createElement('h1', /* ... h1 children ... */),
            React.createElement('ul', /* ... ul children ... */)
        );
```

# JSX tambien es una expresion

Despues de compilarse, las expresiones JSX se convierten en llamadas a funciones JS regulares y se evaluan en objetos JS.
Esto significa que puedes usar JSX dentro de declaraciones **if** y bucles **for**, asignarlo a variables, aceptarlo como argumento, y retornarlo desde dentro de funciones.

```
function getGreeting(user) {
  if(user) {
    return <h1>Hello, {formatName(user)}!<h1/>;
  }
  return <h1>Hello, Stranger.<h1>;
}
```

# Componentes funcionales y de clase

La forma mas sencilla de definir un componente es escribir una funcion de JS:

```
function Welcome(props) {
  return <h1>Hello, {props.name}<h1/>;
}
```
Esta funcion es un componente de React valido porque acepta un solo argumento de objeto "props" con datos y devuelve un elemento de React. LLamamos a dichos componentes "funcionales" porque literalmente son funciones JS.

Tambien puedes utilizar una clase de ES6 para definir un componente:

```
class Welcome extends React.Component {
  render(){
    return <h1>Hello, {this.props.name}</h1>;
  }
}
```
Los dos componentes anteriores son equivalentes desde el punto de vista de React.

## Renderizando un componente

Anteriormente, solo encontramos elementos de React que representan las etiquetas del DOM, sin embargo, los elementos tambien pueden representar componentes definidos por el usuario.
Cuando React ve un elemento representando un componente definido por el usuario, pasa atributos JSX e hijos a este componente como un solo objeto.Llamamos a este objeto "props".

Por ejemplo, este codigo muestra "Hello,Sara" en la pagina:

```
function Welcome(props){
  return <h1>Hello, {props.name}</h1>
}

const root = ReactDOM.createRoot(document.getElementById("root"));
const element = <Welcome name="Sara" />;
root.render(element);
```

1. Llamamos a ```root.render()``` con el elemento ```<Welcome name="Sara" />```.
2. React llama al componente ```Welcome``` con ```{name:"Sara"}``` como "props".
3. Nuestro componente ```Welcome``` devuelve un elemento ```<h1>Hello, Sara</h1>``` como resultado.
4. ReactDOM actualiza eficientemente el DOM para que coincida con ```<h1>Hello, Sara</h1>.```

## Extraccion de componentes

No tengas miedo de dividir los componentes en otros mas pequeños.

Por ejemplo, considerando este componente Comment:

```
function Comment(props) {
  return (
    <div className="Comment">
      <div className="UserInfo">
        <img className="Avatar"
          src={props.author.avatarUrl}
          alt={props.author.name}
        />
        <div className="UserInfo-name">
          {props.author.name}
        </div>
      </div>
      <div className="Comment-text">
        {props.text}
      </div>
      <div className="Comment-date">
        {formatDate(props.date)}
      </div>
    </div>
  );
}
```

Acepta ```author``` (un objeto), ```text```(un string), y ```date```(una fecha) como props, y describe un comentario en una web de redes sociales. Este componente puede ser dificil de cambiar debido a todo el anidamiento, y tambien es dificil reutilizar partes individuales de el. Vamos a extraer algunos componentes del mismo.

Primero, vamos a extraer ```Avatar```:

```
function Avatar(props) {
  return (
    <img className="Avatar"
      src={props.user.avatarUrl}
      alt={props.user.name}
    />
  );
}
```

El  ```Avatar``` no necesita saber que esta siendo renderizado dentro de un ```Comment```. Esto es por lo que le dimos a su propiedad un nombre mas generico: ```user```en vez de ```author```.
**Recomendamos nombrar las props desde el punto de vista del componente, en vez de la del contexto en el que se va a utilizar.**

Ahora podemos simplificar ```Comment``` un poquito:

```
function Comment(props) {
  return (
    <div className="Comment">
      <div className="UserInfo">
        <Avatar user={props.author} />
        <div className="UserInfo-name">
          {props.author.name}
        </div>
      </div>
      <div className="Comment-text">
        {props.text}
      </div>
      <div className="Comment-date">
        {formatDate(props.date)}
      </div>
    </div>
  );
}
```