- React: Biblioteca de JS de codigo abierto diseniada para crear interfaces de usuario

Para crear el entorno:

```
npx create-react-app nombreDeLaCarpeta
```

---------------------------------------

Conceptos basicos:

- **Componente**: Parte de la interfaz de usuario que es independiente y reusable.
Se pueden dividir en dos categorias principales:
1. Funcionales
2. De Clase.

Anteriormente, usabamos componentes de clase para poder trabajar con "estados" de nuestros componentes. En versiones anteriores no podiamos hacerlo en componentes funcionales. Luego vinieron los "Hooks".

- **Hooks**: Funcion especial que nos permite trabajar con estados en componentes funcionales (asignar y actualizar) y otros aspectos de React sin escribir un componente de clase. Esto nos permite escribir codigo mucho mas conciso y facil de entender.
- **Estado (state)**: Representacion en JS del conjunto de propiedades (no se refiere a los props) de un componente y sus valores actuales.
- **Event Listener**: Funcion en JS que es ejecutada cuando ocurre un evento especifico.(tambien podemos referirnos a esta funcion como "Event Handler")

## Funcionales

### Caracteristicas

- Debe retornar un elemento de React (JSX).
- Debe comenzar con una letra mayuscula.
- Puede recibir valores si es necesario.

**Props** : Argumentos que puede recibir un componente de React. Solo pueden ser enviados de "padre a hijo".

```
function Saludo(props){
    return <h1> Hola, {props.nombre}! </h1>;
}
```

```
const DemoComponent = function() {
  return (
    <div className='customClass' />
  );
};
```


## De Clase

### Caracteristicas

- Debe extender React.Component.
- Debe tener un metodo render() para retornar un elemento de JSX.
- Puede recibir valores si es necesario.

```
class Saludo extends React.Component {
    render(){
        return <h1> Hola, {this.props.nombre}!</h1>;
    }
}
```
```
class Kitten extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <h1>Hi</h1>
    );
  }
}
```
Esto crea una clase de ES6 *Kitten* que hereda de la clase **React.Component**. Asi que la clase *Kitten* ahora tiene acceso a muchas caracteristicas utiles de React, como el estado local y el ciclo de vida de los "hooks". 

## Introduccion a JSX (Javascript XML)

Es una extension de React para la sintaxis de JS. Nos permite describir en JS como se veran (su estructura, no necesariamente su estilo) los componentes usando una estructura similar a HTML.

### Ventajas de JSX

- Estructura mas facil de visualizar.
- Errores y advertencias mas utiles.

### Elementos en JSX

- Elemento: Unidades mas pequenias en React. Definen lo que se ve en la pantalla. Los *componentes* en React estan "hechos" de elementos, por ende, los elementos son mas "basicos".

Se reconoce la diferencia entre un elemento y un componente en base a sus etiquetas. En JSX, los **elementos** HTML se representan con etiquetas en letras **minusculas** (por ej, un < div >) en cambio, los **componentes** definidos por el usuario comienzan con una letra **mayuscula** por convencion (por ej, un < Boton >). Al igual que en HTML en JSX podemos agregar **atributos**(clases,id, etc) a tus elementos para especificar ciertas caracteristicas, pero algunos se escriben de forma distinta. Principalmente en las clases. En HTML se escribe en minuscula (*class*) pero en JSX se escribe *className* por que *class* es una palabra reservada en JS. Tambien sucede con el for, por ej:

```
<label for= "css"> CSS </label> // HTML
```
```
<label htmlFor= "css"> CSS </label> // JSX
```

El atributo **style** acepta un objeto JS con propiedades CSS escritas en **camelCase**. Por ej:

```
background-color   // CSS
```
```
backgroundColor   // JSX
```

# Renderizar elementos HTML al DOM

Con react podemos renderizar este JSX directamente al DOM HTML usando la API de renderizado de React conocida como ReactDOM.
ReactDOM ofrece un metodo simple para renderizar elementos React al DOM que se ve asi:

```
ReactDOM.render(componentToRender, targetNode),
```

donde el primer argumento es el elemento o componente React que deseas renderizar, y el segundo argumento es el nodo DOM al que se quiere renderizar el componente.
Como era de esperarse, ReactDOM.render() debe llamarse despues de las declaraciones de los elementos JSX, al igual que hay que declarar las variables antes de usarlas.

### Renderizar componentes

```
<div id="root></div>   // En el archivo HTML 
```

```
const elemento = <h1> Hola, Mundo!</h1>;

ReactDOM.render(
    elemento,
    document.getElementById ('root')
);
```

```
import ReactDOM from 'react-dom';
```

### Javascript en JSX

```
let adjetivo = "Interesante";

<p> React es {adjetivo} </p>
```
Otro ejemplo:

```
let nombre = "Gino";

<p> Su nombre es: {nombre.toUpperCase()} </p>    // GINO
```

---------------

### Algunas diferencias entre HTML y JSX

Una diferencia clave en JSX es que ya no puedes usar la palabra **class** para definir clases HTML.Esto es debido a que **class** es una palabra reservada en JS. En su lugar, JSX utiliza **className**.
De hecho, la convencion de nomenclatura para todos los atributos HTML y referencias a eventos en JSX se convierte a camelCase. Por ejemplo, un evento de clic en JSX es **onClick**, en lugar de **onclick**. Del mismo modo, **onchange** se convierte en **onChange**. 


# Crear un componente con composicion

Imagina que estas construyendo una aplicacion y has creado tres componentes: un **Navbar**, **Dashboard** y **Footer**.
Para componer estos componentes juntos, se podria crear un componente **App** *parent* que renderiza cada uno de estos tres componentes como *children*. Para renderizar un componente como hijo de un componente React, se incluye el nombre del componente escrito como una etiqueta HTML personalizada en el JSX. Por ejemplo, el metodo **render** se puede escribir:

```
return(
    <App>
        <Navbar />
        <Dashboard />
        <Footer />
    </App>
)
```

# Pasar "props" a un componente funcional sin estado

En React, se pueden pasar props, o propiedades a componentes hijos. Tienes un componente App que devuelve un componente hijo llamado **Welcome**, el cual es un componente funcional sin estado. Puedes pasarle una propiedad llamada **user** a **Welcome** escribiendo:

```
<App>
    <Welcome user="Mark"/>
</App>
```

Se puede utilizar atributos personalizados de HTML creados por ti y soportados por React para ser pasados por props a tu componente. En este caso, la propiedad creada **user** es pasada como atributo al componente **Welcome**.
Dado que **Welcome** es un componente funcional sin estado, tiene acceso a este valor de la siguiente manera:

```
const Welcome = (props) => <h1> -hello, {props.user}!</h1>
```

Este valor es llamado **props** por convencion y, cuando se trata de componentes funcionales sin estado, se lo considera como un argumento pasado a una funcion que retorna JSX. Puedes acceder el valor del argumento en el cuerpo de la funcion. En los componentes de clase, esto es un poco diferente.

## Props predeterminadas

Puedes asignar props predeterminadas a un componente como si fueran una propiedad dentro del propio componente y React asigna la prop predeterminada si es necesario. Esto te permite especificar cual debe ser el valor de una prop si no se provee un valor especifico. Por ejemplo:

```
MyComponent.defaultProps = {location: "San Francisco" }
```

Se considera una muy buena practica definir los **propTypes** cuando conoces el tipo de una propiedad con antelacion. Puedes definir una propiedad **propTypes** para un componente de la misma manera que defines **defaultProps**. Al hacer esto, se validara que las props de una clave determinada estan presentes con un tipo especifico. Aqui un ejemplo para validar el tipo **function** para una prop llamada **handleClick**:

```
MyComponent.propTypes = { handleClick: PropTypes.func.isRequired }
```

En el ejemplo, la parte de **PropTypes.func** verifica que **handleClick** es una funcion. Añadir **isRequired** le dice a React que **handleClick** es una propiedad obligatoria para ese componente. **func** representa **function**. 
Nota: A partir de React v15.5.0, **PropTypes** se importa de manera independiente de React, asi: **import PropTypes from 'prop-types';**.

# Acceder a propiedades "props" usando this.props

¿Que pasa si el componente hijo al que se le pasa una propiedad es un componente de clase ES6, en lugar de un componente funcional sin estado? 
Cada vez que se hace referencia a un componente de clase en si mismo, se utiliza la palabra clave **this**. Para acceder a las propiedades dentro de un componente de clase, se antepone al codigo que se utiliza para acceder a el con **this**. Por ejemplo, si un componente de clase de ES6 tiene una propiedad llamada **data**, se escribira **{this.props.data}** en JSX.

# Crea un componente de estado y Renderizarlo en la interfaz de usuario

Uno de los temas mas importantes en React es **state**. El estado consiste en cualquier dato que tu aplicacion necesite conocer y que pueda cambiar con el tiempo. Quieres que tus aplicaciones respondan a los cambios de estado y presenten una interfaz de usuario actualizada cuando sea necesario.React ofrece una buena solucion para el manejo de estado de aplicaciones web modernas.
Creas un estado en un componente de React declarando una propiedad **state** en la clase del componente en su **constructor**. Esto inicializa el componente con state cuando se crea. La propiedad **state** debe establecerse en un **object** de Javascript. Declararlo se ve asi:

```
this.state = {

}
```

Una vez que se define el estado inicial de un componente, se puede mostrar cualquier parte del mismo en la interfaz de usuario que se renderiza.Si un componente tiene estado, siempre tendra acceso a los datos en **state** en su metodo **render()**. Puedeces acceder a los datos con **this.state**. Si quieres acceder a un valor de estado dentro del **return** del metodo de renderizacion, tienes que encerrar el valor entre llaves.

Ejemplo:

```
class StatefulComponent extends React.Component{
    constructor(props){
      super(props);

      this.state = {
        name: "Sebastian"
      }
    }
    render(){
      return (
        <div>
          <h1>{this.state.name}</h1>
        </div>
      );
    }
};
```

## Otra manera de renderizar el estado en la interfaz de usuario

Hay otra manera de acceder al **state** de un componente. En el metodo **render()**, antes de la sentencia **return**, se puede escribir Javascript directamente. Por ejemplo, puedes declarar funciones, acceder a datos de **state** o **props**, realizar calculos sobre estos datos, etc. Luego puedes asignar cualquier dato a las variables, a las que tienes acceso en la sentencia **return**.

Ejemplo:

```
class MyComponent extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      name: "Tuvieja"
    }
  }
  render(){
      const name = this.state.name
    return(
      <div>
        <h1>{name}</h1>
      </div>
    );
  }
};
```

# Definir el estado con this.setState

Anteriormente se cubrio el componente **State** y como inicializar el state en el **constructor**. Tambien hay una forma de cambiar el **state** del componente. React proporciona un metodo para actualizar el componente **state** llamado **setState**. El metodo **setState** dentro de tu clase de componente se llama asi: **this.setState()**, pasando un objeto con pares clave-valor. Las claves son tus propiedades de estado y los valores son datos de estado actualizados. Por ejemplo, si estuvieramos almacenando un **username** en estado y quisieramos actualizarlo, se veria asi:

```
this.setState({
  username: "Lewis"
});
```

React espera que nunca modifiques **state** directamente. En su lugar, siempre usa **this.setState()** cuando ocurran cambios de estado. 

Ejemplo:

Hay un elemento **button** en el editor de codigo que tiene un controlador **onClick()**. Este controlador es activado cuando el **button** recibe un evento clic en el navegador, y ejecuta el metodo **handleClick** definido en **MyComponent**.Dentro del metodo **handleClick**, actualiza el componente **state** usando **this.setState()**.

```
class MyComponent extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      name: "Initial State"
    };
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(){
    this.setState({
      name: "React Rocks!"
    })
  }
  render(){
    return(
      <div>
        <button onClick={this.handleClick}> Click Me </button>
        <h1>{this.state.name}</h1>
      </div>
    );
  }
};
```

## Usar el estado para alternar un elemento

A veces puedes necesitar conocer el estado anterior al actualizar el estado. Sin embargo, las actualizaciones del estado pueden ser asíncronas: esto significa que React puede procesar multiples llamadas a **setState()** en una sola actualizacion. Esto significa que no puedes confiar en el valor anterior de **this.state** o **this.props** al calcular el siguiente valor. Por lo tanto, **NO DEBES** usar codigo como este:

```
this.setState({
  counter:this.state.counter + this.props.increment
});
```

En su lugar, debes pasar una funcion a **setState** que te permitira acceder al estado y props. El usar una funcion con **setState** te garantiza que estas trabajando con los valores mas actuales del estado y props. Esto significa que lo anterior debe reescribirse asi:

```
this.setState((state,props)=>({
  counter: state.counter + props.increment
}))
```

Tambien puedes usar un formulario sin **props** si necesitas solo el **state**:

```
this.setState(state => ({
  counter: state.counter + 1
}));
```

Ejemplo: 

En el siguiente ejemplo tenemos un boton que al clickearlo muestra y oculta el h1.

```
class MyComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visibility: false
    };
      this.toggleVisibility = this.toggleVisibility.bind(this)
  }
toggleVisibility(){
  this.setState(state=>{
    if(state.visibility === true){
      return{visibility : false}
    }else{
      return{visibility : true}
    }
  });
}
  render() {
    if (this.state.visibility) {
      return (
        <div>
          <button onClick={this.toggleVisibility}>Click Me</button>
          <h1>Now you see me!</h1>
        </div>
      );
    } else {
      return (
        <div>
          <button onClick={this.toggleVisibility}>Click Me</button>
        </div>
      );
    }
  }
}
```

### Ejercicio contador simple

El ejemplo a continuacion tendra tres botones: uno para incrementar, otro para restar y un reset.

```
class Counter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: 0
    };
    // Cambia el código debajo de esta línea
      this.increment = this.increment.bind(this);
      this.decrement = this.decrement.bind(this);
      this.reset = this.reset.bind(this);
    // Cambia el código encima de esta línea
  }
  // Cambia el código debajo de esta línea
    increment(){
      this.setState(state=>{
        return {count: state.count + 1}
      });
    }
    decrement(){
      this.setState(state=>{
        return {count: state.count - 1}
      });
    }  
    reset(){
      this.setState(state=>{
        return {count: 0}
      })
    }
  // Cambia el código encima de esta línea
  render() {
    return (
      <div>
        <button className='inc' onClick={this.increment}>Increment!</button>
        <button className='dec' onClick={this.decrement}>Decrement!</button>
        <button className='reset' onClick={this.reset}>Reset</button>
        <h1>Current Count: {this.state.count}</h1>
      </div>
    );
  }
};
```

### Counter con componente funcional y useState

Puedes responder a eventos declarando funciones *event handler* dentro  del componente:

```
function MyButton(){
  function handleClick(){
    alert("You clicked me!")
  }

  return(
    <button onClick={handleClick}>
      Click me
    </button>
  );
}
```

Fijarse que en **onClick={handleClick}** no hay parentesis al final. No se llama a la funcion de event handler, solo la necesitas para pasarla.

A menudo, queremos que nuestro componente "recuerde" cierta informacion y la muestre. Por ejemplo, quizas querramos contar la cantidad de clicks que le damos a un boton.Para hacer esto, agregamos state al componente.
Primero, importamos **useState** from React:

```
import { useState } from "react";
```

Ahora declaramos la variable *state* dentro del componente:

```
function MyButton(){
  const [count,setCount] = useState(0);
}
```

Obtendremos dos cosas de **useState**: el state actual (count), y la funcion que te permite actualizarla (setCount). Le podes dar cualquier nombre pero la convencion es llamarlas como [algo,setAlgo].

La primera vez que el boton sea mostrado, count será igual a 0 porque se le paso 0 a useState(). Cuando quieras cambiar el estado, llama setCount() y se le pasa el nuevo valor. Clickear este boton incrementara el contador:

```
function MyButton(){
  const [count,setCount] = useState(0);

  function handleClick(){
    setCount(count + 1);
  }

  return(
    <button onClick={handleClick}>
      Clicked {count} times
    </button>
  );
}
```