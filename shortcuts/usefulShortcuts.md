# VS Code Keyboard Shortcuts

CTRL + K + C : Comment

CTRL + K + U : Uncomment

Alt + Shift + Up Arrow / Down Arrow : Duplicate line above / below

F2 : Rename the variable name

CTRL + D : Multi Cursor, Select next occurrence

CTRL + Shift + L : Multi Cursor, Select All in File

CTRL + Shift + V : Visualize md file

# Generic Windows Shortcuts

Windows + D : Minimize all windows, go to desktop

Windows + Up/Down Arrow : Minimize/ Maximize current window/app

Windows + Left/Right Arrow : Move current window/app to left/right side of/next screen

# Chrome Browser Shortcuts

F12 or CTRL + Shift + i: Open console

CTRL + W : To close the current tab in browser

CTRL + N : Open new browser window whether its Chrome or IE

CTRL + TAB : Switch between different tabs forward in browser

CTRL + SHIFT + TAB : Switch between different tabs backward in browser

CTRL + T : Open new tab in Browser

CTRL + Shift + T : Restore the last closed tab

CTRL + 1/2/3 (number) : Switch to the first/second/third tab

CTRL + D : Bookmark current page (añadir a favoritos)

