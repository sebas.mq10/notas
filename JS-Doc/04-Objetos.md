# Objetos

Los objetos son usados para almacenar colecciones de varios datos y entidades mas complejas asociados con un nombre clave.
Podemos crear un objeto usando las llaves {...} con una lista opcional de *propiedades*. Una prop. es un par "key:value", donde key es un string, y value puede ser cualquier cosa.

```
let user = {     // un objeto
  name: "John",  // En la clave "name" se almacena el valor "John"
  age: 30        // En la clave "age" se almacena el valor 30
};
```

Se puede acceder a los valores de las prop. utilizando la notacion de punto:

```
alert( user.name ); // John
alert( user.age ); // 30
```

## La prueba de prop.existente, el operador "in"

Es posible acceder a cualquier propiedad con el operador "in". La sintaxis es:

```
"key" in object
```
```
let user = { name: "John", age: 30 };

alert( "age" in user );    // mostrará "true", porque user.age sí existe
alert( "blabla" in user ); // mostrará false, porque user.blabla no existe
```

## El bucle "for...in"

Para recorrer todas las claves de un objeto existe una forma especial de bucle: "for...in". Por ej:

```
let user = {
  name: "John",
  age: 30,
  isAdmin: true
};

for (let key in user) {
  // claves
  alert( key );  // name, age, isAdmin
  // valores de las claves
  alert( user[key] ); // John, 30, true
}
```
## Metodos del objeto, "this"

En el mundo real un usuario puede *actuar*: seleccionar algo del carrito de compras, hacer login,logout,etc. Las acciones son representadas en Javascript por funciones en las propiedades.

### Ejemplos de metodos

```
let user = {
  name: "John",
  age: 30
};

user.sayHi = function() {
  alert("¡Hola!");
};

user.sayHi(); // ¡Hola!
```

Una funcion que es la propiedad de un objeto es denominada su *metodo*.

Existe una sintaxis mas corta para los metodos en objetos literales:

```
// estos objetos hacen lo mismo

user = {
  sayHi: function() {
    alert("Hello");
  }
};

// la forma abreviada se ve mejor, ¿verdad?
user = {
  sayHi() {   // igual que "sayHi: function(){...}"
    alert("Hello");
  }
};
```

### "this" en metodos

Es comun que un metodo de objeto necesite acceder a la informacion almacenada en el objeto para cumplir su tarea. Por ej, el codigo dentro de *user.sayHi()* puede necesitar el nombre del usuario *user*.
Para acceder al objeto, un metodo puede usar la palabra clave **this**. El valor de **this** es el objeto "antes del punto", el usado para llamar al metodo. Por ejemplo:

```
let user = {
  name: "John",
  age: 30,

  sayHi() {
    // "this" es el "objeto actual"
    alert(this.name);
  }

};

user.sayHi(); // John
```

En JS, la palabra clave this puede ser usado en cualquier funcion, incluso si no es el metodo de un objeto. El valor de this es evaluado durante el tiempo de ejecucion, dependiendo del contexto.

Por ejemplo, aqui la funcion es asignada a dos objetos diferentes y tiene diferentes "this" en sus llamados:

```
let user = { name: "John" };
let admin = { name: "Admin" };

function sayHi() {
  alert( this.name );
}

// usa la misma función en dos objetos
user.f = sayHi;
admin.f = sayHi;

// estos llamados tienen diferente "this"
// "this" dentro de la función es el objeto "antes del punto"
user.f(); // John  (this == user)
admin.f(); // Admin  (this == admin)

admin ['f'] (); // Admin (punto o corchetes para acceder al método, no importa)
```

La regla es simple: si obj.f() es llamado, entonces this es obj durante el llamado de f. Entonces es tanto user o admin en el ejemplo anterior.

(Las funciones flechas no tienen this).



## Constructor, operador "new"

La sintaxis habitual {...} nos permite crear un objeto. Pero a menudo necesitamos crear varios objetos similares, como multiples usuarios, elementos de menu, etc. Esto se puede realizar utilizando el constructor de funciones y el operador "new".

### Funcion constructora

La funcion constructora es tecnicamente una funcion normal. Aunque hay dos convenciones:

1. Son nombradas con la primera letra mayuscula.
2. Solo deben ejecutarse con el operador "new". 

Por ej:

```
function User(name) {
  this.name = name;
  this.isAdmin = false;
}

let user = new User("Jack");

alert(user.name); // Jack
alert(user.isAdmin); // false
```

Cuando una funcion es ejecutada con new, realiza los siguientes pasos:

1. Se crea un nuevo objeto vacio y se asigna a this.
2. Se ejecuta el cuerpo de la funcion. Normalmente se modifica this y se le agrega nuevas propiedades.
3. Se devuelve el valor de this.

### Return desde constructores

Normalmente los constructores no tienen sentencia return, pero si hay, entonces la regla es simple:

- Si return es llamado con un objeto, entonces se devuelve tal objeto en vez de this.
- Si return es llamado con un tipo de dato primitivo, es ignorado.

En otras palabras, return con un objeto devuelve ese objeto, en todos los demas casos se devuelve this. 

Por ejemplo, aqui return anula this al devolver un objeto:

```
function BigUser() {

  this.name = "John";

  return { name: "Godzilla" };  // <-- devuelve este objeto
}

alert( new BigUser().name );  // Godzilla, recibió ese objeto
```

### Metodos en constructor

Utilizar constructor de funciones para crear objetos nos da mucha flexibilidad. La funcion constructor puede tener argumentos que definan como construir el objeto y que colocar dentro.
Por supuesto podemos agregar a this no solo a propiedades, sino tambien metodos.

Por ejemplo, new User(name) de abajo, crea un objeto con el name dado y el metodo sayHi:

```
function User(name) {
  this.name = name;

  this.sayHi = function() {
    alert( "Mi nombre es: " + this.name );
  };
}

let john = new User("John");

john.sayHi(); // Mi nombre es: John

/*
john = {
   name: "John",
   sayHi: function() { ... }
}
*/
```

## Encadenamiento opcional "?"

La sintaxis de encadenamiento opcional ?. tiene tres formas:

1. obj?.prop - devuelve obj.prop si obj existe, si no, undefined.
2. obj?.[prop] - devuelve obj[prop] si obj existe, si no, undefined.
3. obj.method?.() - llama a obj.method() si obj.method existe, si no devuelve undefined.

 El ?. comprueba si la parte izquierda es null/undefined y permite que la evaluación continúe si no es así.

Una cadena de ?. permite acceder de forma segura a las propiedades anidadas.

Aún así, debemos aplicar ?. con cuidado, solamente donde sea aceptable que, de acuerdo con nuestra lógica, la parte izquierda no exista. Esto es para que no nos oculte errores de programación, si ocurren.


## Tipo Symbol

Segun la especificacion, solo dos de los tipos primitivos pueden servir como clave de propiedad de objetos:

- String, o
- symbol

Si se usa otro tipo, como un número, este se autoconvertirá a string. Así, obj[1] es lo mismo que obj["1"], y obj[true] es lo mismo que obj["true"].

El valor de "Symbol" representa un identificador unico. Un valor de este tipo puede ser creado usando Symbol():

```
let id = Symbol();
```

También le podemos agregar una descripción (también llamada symbol name), que será útil en la depuración de código:

```
// id es un symbol con la descripción "id"
let id = Symbol("id");
```
Se garantiza que los simbolos son unicos. Aunque declaremos varios Symbols con la misma descripcion, estos tendran valores distintos. La descripcion es solamente una etiqueta que no afecta nada mas.
Por ejemplo, aqui hay dos Symbols con la misma descripcion - pero no son iguales:

```
let id1 = Symbol("id");
let id2 = Symbol("id");

alert(id1 == id2); // false
```

- **Symbols no se autoconvierten a String**

Por ejemplo, este alert mostrara un error:

```
let id = Symbol("id");
alert(id); // TypeError: No puedes convertir un valor Symbol en string
```
- **Si realmente queremos mostrar un Symbol, necesitamos llamar el método .toString() explícitamente:**

```
let id = Symbol("id");
alert(id.toString()); // Symbol(id), ahora sí funciona
```

### Symbols se utiliza principalmente:

1. Propiedades de objeto "Ocultas"

Si queremos agregar una propiedad a un objeto que “pertenece” a otro script u otra librería, podemos crear un symbol y usarlo como clave. Una clave symbol no aparecerá en los ciclos for..in, por lo que no podrá ser procesada accidentalmente junto con las demás propiedades. Tampoco puede ser accesada directamente, porque un script ajeno no tiene nuestro symbol. Por lo tanto la propiedad estará protegida contra uso y escritura accidentales.

Podemos “ocultar” ciertos valores dentro de un objeto que solo estarán disponibles dentro de ese script usando las claves de symbol.


## Conversion de objeto a valor primitivo

¿Que sucede cuando los objetos se suman obj1 + obj2, se restan obj1 - obj2 o se imprimen utilizando alert(obj)?

En ese caso, los objetos se convierten automaticamente en valores primitivos, y luego se lleva a cabo la operacion sobre esos primitivos, y resultan en un valor primitivo.

Esto es una limitacion importante: el resultado de obj1 + obj2 (u otra operacion) **¡no puede ser otro objeto!**

No se hacen matematicas con objetos en proyectos reales.

### Reglas de conversion

1. No hay conversion a boolean. Todos los objetos son true en un contexto booleano, tan simple como eso. Solo hay conversiones numericas y de strings.
2. La conversion numerica ocurre cuando restamos objetos o aplicamos funciones matematicas. Por ejemplo, los objetos de tipo Date se pueden restar, y el resultado de date1 - date2 es la diferencia horaria entre dos fechas.
3. En cuanto a la conversion de strings: generalmente ocurre cuando imprimimos un objeto como en alert(obj) y en contextos similares.

Hay 3 tipos (hints o sugerencias) de estas:

- "string" (para alert y otras operaciones que necesitan un string)
- "number" (para matemáticas)
- "default" (pocos operadores, usualmente los objetos lo implementan del mismo modo que "number")

La especificación describe explícitamente qué operador utiliza qué sugerencia.

El algoritmo de conversión es:

1. Llamar a obj[Symbol.toPrimitive](hint) si el método existe,
2. En caso contrario, si la sugerencia es "string"
- intentar llamar a obj.toString() y obj.valueOf(), lo que exista.
3. En caso contrario, si la sugerencia es "number" o "default"
- intentar llamar a obj.valueOf() y obj.toString(), lo que exista.
Todos estos métodos deben devolver un primitivo para funcionar (si está definido).

En la práctica, a menudo es suficiente implementar solo obj.toString() como un método “atrapatodo” para todas las conversiones a string que deben devolver la representación “legible por humanos” de un objeto, con fines de registro o depuración.

Como en las operaciones matemáticas, JavaScript no ofrece una forma de “sobrescribir” operadores usando métodos. Así que en proyectos de la vida real raramente se los usa en objetos.