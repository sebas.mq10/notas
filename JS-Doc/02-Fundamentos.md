# Operadores basicos

## Resto %

El resultado de a % b es el resto de la division entera de a por b.

```
alert (5 % 2); // 1 es un resto de 5 dividido por 2
alert (8 % 3); // 2 es un resto de 8 dividido por 3
```

## Exponenciacion

```
alert (2 ** 2); // 2 al cuadrado = 4
alert (2 ** 3); // 2 al cubo = 8
alert (2 ** 4); // 2 a la cuarta = 16

alert (4 ** 1/2); // 2 (potencia de 1/2 es lo mismo que raiz cuadrada)
```

## Concatenacion de cadenas con el binario +

Si se aplica el + binario a una cadena, los une (concatena):

```
let s = "my" + "string";
alert (s); = mystring
```

Si uno de los operandos es una cadena, el otro es convertido a una cadena tambien:

```
alert ('1' + 2); // "12"

alert (2 + 2 + '1'); // "41" y no "221"
```

El binario + es el unico operador que soporta cadenas en esa forma. Otros operadores matematicos trabajan solamente co numeros y siempre convierten sus operandos a numeros.

Por ejemplo, resta y division:

```
alert (2 - '1'); // 1
alert ('6' / '2'); // 3
```

## Conversion numerica, unario + 

```
// Sin efecto en números
let x = 1;
alert( +x ); // 1

let y = -2;
alert( +y ); // -2

// Convierte los no números
alert( +true ); // 1
alert( +"" );   // 0
```

## Incremento/ Decremento

- **Incremento** ++ incrementa una variable por 1:

```
let counter = 2;
counter++;   // funciona igual que counter = counter + 1

alert (counter); // 3
```

- **Decremento** -- decrementa una variable por 1:

```
let counter = 2;
counter--;   // funciona igual que counter = counter - 

alert (counter);  // 1
```

Los operadores ++ y -- pueden ser colocados antes o despues de una variable.

- Cuando el operador va despues de la variable, esta en "forma de sufijo": counter++.
- Cuando el operador va antes de la variable, esta en "forma de prefija": ++counter.

La forma sufijo devuelve el valor anterior (antes del incremento/decremento) mientras que la forma prefijo devuelve el nuevo valor.

Forma sufijo:

```
let counter = 1;
let a = counter++; // (*) cambiado ++counter a counter++

alert (a); 1
```

En la linea (*), la forma sufijo counter++ incrementa counter pero devuelve el *antiguo* valor (antes de incrementar). Por lo tanto, el alert muestra 1.

Forma prefijo:

```
let counter = 1;
let a= ++counter; // (*)

alert(a); // 2
```

En la linea (*), la forma prefijo ++counter incrementa counter y devuelve el nuevo valor, 2. Por lo tanto, el alert muestra 2.


# Ejecucion condicional: if, '?'

La sentencia if(...) evalua la condicion en los parentesis, y si el resultado es **true** ejecuta un bloque de codigo.
Por ejemplo:

```
let year = prompt ('En que anio naci?)
if (year == 1989) alert ('Es cierto)
```

Si queremos ejecutar mas de una sentencia se usan llaves.

## La clausula "else"

La sentencia if quizas contenga un bloque "else" opcional. Este se ejecutara cuando la condicion sea falsa.

```
let year = prompt ('En que anio fue publicada la especificacion ECMAScript-2015?','');

if(year == 2015){
    alert ('Lo adivinaste, correcto!');
} else {
    alert ('Como puedes estar tan equivocado?'); // cualquier valor excepto 2015
}
```

## Else if

Algunas veces, queremos probar variantes de una condicion. La clausula else if nos permite hacer esto. Por ej:

```
let year = prompt('¿En qué año fue publicada la especificación ECMAScript-2015?', '');

if (year < 2015) {
  alert( 'Muy poco...' );
} else if (year > 2015) {
  alert( 'Muy Tarde' );
} else {
  alert( '¡Exactamente!' );
}
```

## Operador ternario "?"

La sintaxis es:

```
let result = condition ? value1 : value2;
```

Se evalua condition: si es verdadera entonces devuelve value1, de lo contrario value2. Por ejemplo:

```
let accessAllowed = (age > 18) ? true : false;
```

# Operadores Logicos

## || (OR)

```
result = a || b;
```

Si cualquiera de sus argumentos es true, retorna true,de lo contrario retorna false.

Hay cuatro combinaciones logicas posibles:

```
alert(true || true); // true (verdadero)
alert(false || true); // true
alert(true || false); // true
alert(false || false); // false (falso)
```

El resultado es siempre true excepto cuando ambos operandos son false.

La mayoria de las veces, OR || es usado en una declaracion if para probar si alguna de las condiciones dadas es true.

```
let hour = 9;

if(hour < 10 || hour > 18){
  alert("la oficina esta cerrada.");
}
```

```
let hour = 12;
let isWeekend = true;

if (hour < 10 || hour > 18 || isWeekend) {
  alert ("La oficina esta cerrada."); // Es fin de semana
}
```

## OR "||" encuentra el primer valor verdadero

**Una cadena de OR devuelve el primer valor verdadero o el ultimo si ningun verdadero es encontrado.**

```
alert(1 || 0); // 1 (1 es un valor verdadero)

alert(null || 1); // 1 (1 es el primer valor verdadero)
alert(null || 0 || 1); // 1 (el primer valor verdadero)

alert(undefined || null || 0); // 0 (todos son valores falsos, retorna el último valor)
```

Esto brinda varios usos interesantes:

1. **Obtener el primer valor verdadero de una lista de variables o expresiones.**

Por ejemplo, tenemos tres variables, todas opcionales (pueden ser undefined o tener valores falsos). Usamos OR || para elegir el que tiene datos y mostrarlo (o anonimo si no hay nada configurado.):

```
let firstName = "";
let lastName = "";
let nickName = "SuperCoder";

alert(firstName || lastName || nickName || "Anonymous"); // SuperCoder
```

Si todas las variables fueran falsas, apareceria "Anonymous".

2. **Evaluacion del camino mas corto.**

Esto significa que || procesa sus argumentos hasta que se alcanza el primer valor verdadero, y ese valor se devuelve inmediatamente sin siquiera tocar el otro argumento.
En el siguiente ejemplo, solo se imprime el segundo mensaje:

```
true || alert("not printed")
false || alert("printed")
```

En la primera linea, el operador OR || detiene la evaluacion inmediatamente despues de ver que es verdadera, por lo que el alert no se ejecuta.
A veces se usa esta funcion para ejecutar comandos solo si la condicion en la parte izquierda es falsa.

## && (AND)

```
result = a && b;
```

AND retorna **true** si ambos operandos son valores verdaderos y **false** en cualquier otro caso.

```
alert(true && true); // true;
alert(false && true); // false;
alert(true && false); // false;
alert(false && false) // false;
```
Un ejemplo con **if**:

```
let hour = 12;
let minute = 30;

if(hour == 12 && minute == 30){
  alert("La hora es 12:30");
}
```

## AND "&&" encuentra el primer valor falso.

Dado multiples valores aplicados al operador AND:

```
result = value1 && value2 && value3;
```

El operador AND realiza lo siguiente:

- Evalua los operandos de izquierda a derecha.
- Para cada operando, los convierte a un booleano. Si el resultado es false, se detiene y retorna el valor original de dicho operando.
- Si todos los operandos han sido evaluados (todos fueron valores verdaderos), retorna el ultimo operando.

Las reglas anteriores son similares a las de OR. La diferencia es que AND retorna el primer valor falso mientras que OR retorna el primer valor *verdadero.*

```
// si el primer operando es un valor verdadero,
// AND retorna el segundo operando:
alert(1 && 0); // 0
alert(1 && 5); // 5

// si el primer operando es un valor falso,
// AND lo retorna. El segundo operando es ignorado
alert(null && 5); // null
alert(0 && "cualquier valor"); // 0
```

Tambien podemos pasar varios valores de una vez. El primer valor falso es retornado:

```
alert(1 && 2 && null && 3); // null
```

Cuando todos los valores son verdaderos, el ultimo valor es retornado:

```
alert(1 && 2 && 3); // 3, el ultimo.
```

**La precedencia de AND && es mayor que la de OR ||.**

Asi que el codigo **a && b || c && d** es basicamente el mismo que si la expresiones **&&** estuvieran entre parentesis: **(a && b) || (c && d).**

## ! (NOT)

La sintaxis es bastante simple:

```
result = !value;
```

El operador acepta un solo argumento y realiza lo siguiente:

1. Convierte el operando al tipo booleano: **true/false**.
2. Retorna el valor contrario.

Por ejemplo:

```
alert(!true); // false
alert(!0); // true
```

Un doble NOT **!!** es a veces usado para convertir un valor al tipo booleano:

```
alert(!!"cadena de texto no vacia"); // true
alert(!!null); // false
```

La precedencia de NOT **!** es la mayor de todos los operadores logicos, asi que siempre se ejecuta primero, antes que **&&** o **||**.

# Operador Nullish Coalescing '??'

El operador "nullish coalescing (fusion de null) se escribe con un doble signo de cierre de interrogacion **??**.
Como este trata a **null** y a **undefined** de forma similar, usaremos un termino especial para este articulo.Diremos que una expresion es "definida" cuando no es **null** ni **undefined**.

El resultado de **a ?? b**:

* si **a** esta "definida", sera **a**.