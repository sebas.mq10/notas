# Metodos en tipos primitivos

### Diferencias entre primitivos y objetos

Un primitivo

- Es un valor de tipo primitivo
- Hay 7 tipos primitivos: *string, number, bigint,boolean, symbol, null y undefined*.

Un objeto

- Es capaz de almacenar multiples valores como propiedades
- Puede ser creado con {}. Ejemplo: {name: "John", age: 30}. Hay otras clases de objetos en JS; las funciones, por ej, son objetos.

Una de las mejores cosas de los objetos es que podemos almacenar una funcion como una de sus propiedades.


```
let john = {
  name: "John",
  sayHi: function() {
    alert("Hi buddy!");
  }
};

john.sayHi(); // Hi buddy!
```
Aqui hemos creado un objeto *John* con el metodo *sayHi*.

# Numeros

```
let billion = 1000000000;
```

Tambien se puede escribir de esta manera para hacerlo mas legible:

```
let billion = 1_000_000_000;
```

En JS, acortamos un numero agregando la letra "e" y especificando la cantidad de ceros:

```
let billion = 1e9;  // 1 billion, literalmente: 1 y 9 ceros

alert( 7.3e9 );  // 7.3 billions (tanto 7300000000 como 7_300_000_000)
```

En otras palabras, "e" multiplica el numero por el 1 seguido de la cantidad de ceros dada.

```
1e3 === 1 * 1000; // e3 significa *1000
1.23e6 === 1.23 * 1000000; // e6 significa *1000000
```

Ahora por ejemplo 1 microsegundo:

```
let mсs = 0.000001;
```

Igual que antes, el uso de la "e" puede ayudar. Se podria expresar lo mismo como:

```
let mcs = 1e-6; // seis ceros a la izquierda de 1
```

Un nro negativo detras de "e" significa una division por el 1 seguido de la cantidad dada de ceros:

```
// -3 divide por 1 con 3 ceros
1e-3 === 1 / 1000; // 0.001

// -6 divide por 1 con 6 ceros
1.23e-6 === 1.23 / 1000000; // 0.00000123
```

# Strings 

Los Strings pueden estar entre comillas simples, comillas dobles o backticks(``). Estos ultimos, nos permiten ingresar expresiones dentro del string, envolviendolos en ${...}

```
function sum(a, b) {
  return a + b;
}

alert(`1 + 2 = ${sum(1, 2)}.`); // 1 + 2 = 3.
```
- **Strings son inmutables**

- Para obtener un caracter, usa [ ]
- Para obtener un substring, usa *slice* o *substring*

Existen muchas formas de buscarpor subcadenas de caracteres dentro de una cadena completa. El primer metodo es *str.indexOf (substr,pos)*.

Este busca un **substr** en **str**, comenzando desde la posicion entregada **pos**, y retorna la posicion donde es encontrada la coincidencia o -1 en caso de no encontrar nada. Por ej:

```
let str = "Widget con id";

alert (str.indexOf("Widget")); // 0,ya que 'widget', es encontrado al comienzo.

alert (str.indexOf("widget")); // -1, no es encontrado, la busqueda tiene en cuenta minusculas y mayusculas.

alert (str.indexOf('id')); // 1, "id" es encontrado en la posicion 1 (..idget con id)
```

# Arrays

1. Para agregar/remover elementos:
- La propiedad *length* es la longitud del array.
- *push (..items)* agrega items al final.
- *pop ()* remueve el elemento del final y lo devuelve.
- *shift ()* remueve el elemento del principio y lo devuelve.
- *unshift (..items)* agrega items al principio.
- *splice (pos,deleteCount,...items)* desde el indice pos borra deleteCount elementos e inserta items.
- *slice (start, end)* crea un nuevo array y copia elementos desde la posicion start hasta end (no incluido) en el nuevo array.
*concat (..items) devuelve un nuevo array: copia todos los elementos del array actual y le agrega items. Si alguno de los items es un array, se toman sus elementos.

2. Para buscar entre elementos:
- *indexOf / lastIndexOf(item,pos)* busca por item comenzando desde la posicion pos, devolviendo el indice o -1 si no se encuentra.
- *includes(value)* devuelve true si el array tiene value, sino false.
- *find/filter(func)* filtra elementos a traves de la funcion, devuelve el primer/todos los valores que devuelven true.
- *findIndex* es similar a find pero devuelve el indice en lugar del valor.

3. Para iterar sobre elementos:
- *forEach(func)* llama la func para cada elemento, no devuelve nada.
- for (let i=0; i< arr.lengt; i ++) -- lo mas rapido, compatible con viejos navegadores.
- for (let item of arr) -- la sintaxis moderna para items solamente.

4. Para transformar el array:
- *map(func)* crea un nuevo array a partir de los resultados de llamar a la func para cada elemento.
- *sort(func)* ordena el array y lo devuelve.
- *reverse( )* ordena el array de forma inversa y lo devuelve.
- *split/join* convierte una cadena en un array y viceversa.
- *reduce/reduceRight (func, initial)* calcula un solo valor para todo el array, llamando a la func para cada elemento, obteniendo un resultado parcial en cada llamada y pasandolo a la siguiente.

5. Adicional:
- *Array.isArray(arr)* comprueba si arr es un array.
- *arr.some(fn)/arr.every(fn)* comprueba el array.
La funcion fn es llamada para cada elemento del array de manera similar a map. Si alguno/todos los resultados son true, devuelve true, sino, false. Estos metodos se comportan con similitud a los operadores || y &&: si fn devuelve un valor verdadero, *arr.some()* devuelve true y detiene la iteracion de inmediato; si fn devuelve un valor falso, *arr.every()* devuelve false y detiene la iteracion tambien.
- *arr.fill(value,start,end)* llena el array repitiendo value desde el indice start hasta end.
- * arr.copyWithin(target,start,end)* copia sus elementos desde la posicion start hasta la posicion end en si mismo, a la posicion target (reescribe lo existente).
- *arr.flat(depth)/arr.flatMap(fn)* crea un nuevo array plano desde un array multidimensional.

## Iterables

Los objetos iterables son una generalizacion de arrays. Es un concepto que permite que cualquier objeto pueda ser utilizado en un bucle for.. of.
Por ejemplo:

```
let range = {
  from: 1,
  to: 5
};

// Queremos que el for..of funcione de la siguiente manera:
// for(let num of range) ... num=1,2,3,4,5
```

Para hacer que el objeto *range* sea iterable (y asi permitir que for..of funcione) necesitamos agregarle un metodo llamado *Symbol.iterator* (un simbolo incorporado especial usado solo para realizar esa funcion).

1. Cuando se inicia for..of, este llama al metodo Symbol.iterator una vez (o genera un error si no lo encuentra). El metodo debe devolver un iterador: un objeto con el metodo next().
2. En adelante, for..of trabaja *solamente* con ese objeto devuelto.
3. Cuando for..of quiere el siguiente valor,llama a next() en ese objeto.
4. El resultado de next() debe tener la forma **{done: Boolean, value: any}**, donde **done=true** significa que el bucle ha finalizado; de lo contrario, el nuevo valor es value.

Aqui esta la implementacion completa de range:

```
let range = {
  from: 1,
  to: 5
};

// 1. Una llamada a for..of inicializa una llamada a esto:
range[Symbol.iterator] = function() {

  // ... devuelve el objeto iterador:
  // 2. En adelante, for..of trabaja solo con el objeto iterador debajo, pidiéndole los siguientes valores
  return {
    current: this.from,
    last: this.to,

    // 3. next() es llamado en cada iteración por el bucle for..of
    next() {
      // 4. debe devolver el valor como un objeto {done:.., value :...}
      if (this.current <= this.last) {
        return { done: false, value: this.current++ };
      } else {
        return { done: true };
      }
    }
  };
};

// ¡Ahora funciona!
for (let num of range) {
  alert(num); // 1, luego 2, 3, 4, 5
}
```

Note una caracteristica fundamental de los iterables: separacion de conceptos. 

- El range en si mismo no tiene el metodo *next()*.
- En cambio, la llamada a range [ Symbol.iterator ] () crea otro objeto llamado "iterador", y su next() genera valores para la iteracion.

Por lo tanto, el objeto iterador esta separado del objeto sobre el que itera. Tecnicamente podriamos fusionarlos y usar el range mismo como iterador para simplificar el codigo, de esta manera:


```
let range = {
  from: 1,
  to: 5,

  [Symbol.iterator]() {
    this.current = this.from;
    return this;
  },

  next() {
    if (this.current <= this.to) {
      return { done: false, value: this.current++ };
    } else {
      return { done: true };
    }
  }
};

for (let num of range) {
  alert(num); // 1, luego 2, 3, 4, 5
}
```

### String es iterable

Los arrays y strings son los iterables integrados mas utilizados. En un string el bucle for..of recorre sus caracteres:

```
for (let char of "test") {
  // Se dispara 4 veces: una vez por cada carácter
  alert( char ); // t, luego e, luego s, luego t
}
```

### Iterables y simil-array (array-like)

Los dos son terminos oficiales que se parecen, pero son muy diferentes:

- *Iterables* son objetos que implementan el metodo Symbol.iterator, como se describio anteriormente
- *simil-array* son objetos que tienen indices y longitud o *length*, por lo que se "ven" como arrays.

Cuando usamos JS podemos encontrar objetos que son iterables o array-like, o ambos. Por ejemplo, los strings son iterables (for..of funciona en ellas) y array-like (tienen indices numericos y length). Pero un iterable puede que no sea array-like y viceversa, un array-like puede no ser iterable.
Por ejemplo, range en el ejemplo anterior es iterable, pero no es array-like por que no tiene propiedades indexadas ni length.

Y aqui el objeto tiene forma de array, pero no es iterable:

```
let arrayLike = { // tiene índices y longitud => array-like
  0: "Hola",
  1: "Mundo",
  length: 2
};

// Error (sin Symbol.iterator)
for (let item of arrayLike) {}
```

Tanto los iterables como los array-like generalmente no son arrays, no tienen “push”, “pop”, etc. Eso es bastante inconveniente si tenemos un objeto de este tipo y queremos trabajar con él como con una matriz. P.ej. nos gustaría trabajar con range utilizando métodos de matriz. ¿Cómo lograr eso?

### Array.from

Existe un metodo universal Array.from que toma un valor iterable o simil-array y crea un Array "real" a partir de el. De esta manera podemos llamar y usar metodos que pertenecen a un array. Por ejemplo:

```
let arrayLike = {
  0: "Hola",
  1: "Mundo",
  length: 2
};

let arr = Array.from(arrayLike); // (*)
alert(arr.pop()); // Mundo (el método pop funciona)
```

Array.from en la linea (*) toma el objeto, y si es iterable o simil-array crea un nuevo array y copia alli todos los elementos. Lo mismo sucede para un iterable:

```
// suponiendo que range se toma del ejemplo anterior
let arr = Array.from(range);
alert(arr); // 1,2,3,4,5 (la conversión de matriz a cadena funciona)
```

# Map y Set

## Map 

Map es, al igual que Object, una coleccion de datos identificados por claves. Pero la principal diferencia es que Map permite claves de cualquier tipo.
Los metodos y propiedades son:

- new Map()  --- crea el mapa.
- map.set (clave,valor) --- almacena el valor asociado a la clave.  
- map.get (clave) --- devuelve el valor de la clave. Sera undefined si la clave no existe en map.
- map.has (clave) --- devuelve true si la clave existe en map, false si no existe.
- map.delete (clave) --- elimina el valor de la clave.
- map.clear () --- elimina todo de map.
- map.size --- tamanio, devuelve la cantidad actual de elementos.

Por ejemplo:

```
let map = new Map();

map.set('1', 'str1');   // un string como clave
map.set(1, 'num1');     // un número como clave
map.set(true, 'bool1'); // un booleano como clave

// ¿recuerda el objeto regular? convertiría las claves a string.
// Map mantiene el tipo de dato en las claves, por lo que estas dos son diferentes:
alert( map.get(1)   ); // 'num1'
alert( map.get('1') ); // 'str1'

alert( map.size ); // 3
```

Podemos ver que, a diferencia de los objetos, las claves no se convierten en strings. Cualquier tipo de clave es posible en un Map.

**Tambien podemos usar objetos como claves.** Por ejemplo:

```
let john = { name: "John" };

// para cada usuario, almacenemos el recuento de visitas
let visitsCountMap = new Map();

// john es la clave para el Map
visitsCountMap.set(john, 123);

alert( visitsCountMap.get(john) ); // 123
```

### Encadenamiento

Cada llamada a map.set devuelve map en si, asi que podemos "encadenar" las llamadas:

```
map.set('1', 'str1')
   .set(1, 'num1')
   .set(true, 'bool1');
```

### Iteracion sobre Map

Para recorrer un map, hay 3 metodos:

- map.keys () --- devuelve un iterable para las claves.
- map.values () --- devuelve un iterable para los valores
- map.entries () --- devuelve un iterable para las entradas [clave, valor]. Es el que usa por defecto en for..of.

Por ejemplo:

```
let recipeMap = new Map([
  ['pepino', 500],
  ['tomates', 350],
  ['cebollas',    50]
]);

// iterando sobre las claves (verduras)
for (let vegetable of recipeMap.keys()) {
  alert(vegetable); // pepino, tomates, cebollas
}

// iterando sobre los valores (precios)
for (let amount of recipeMap.values()) {
  alert(amount); // 500, 350, 50
}

// iterando sobre las entradas [clave, valor]
for (let entry of recipeMap) { // lo mismo que recipeMap.entries()
  alert(entry); // pepino,500 (etc)
}

```

Ademas, Map tiene un metodo forEach incorporado, similar al de Array:


```
// recorre la función para cada par (clave, valor)
recipeMap.forEach( (value, key, map) => {
  alert(`${key}: ${value}`); // pepino: 500 etc
});
```

### Object.entries: Map desde Objeto

Al crear un Map, podemos pasarle un array (u otro iterable) con pares clave/valor para la inicialización:

```
// array de [clave, valor]
let map = new Map([
  ['1',  'str1'],
  [1,    'num1'],
  [true, 'bool1']
]);

alert( map.get('1') ); // str1
```

Si tenemos un objeto plano y queremos crear un Map a partir de el, podemos usar el metodo incorporado Object.entries (obj) que devuelve un array de pares clave/valor para un objeto exactamente en ese formato. 

Entonces podemos inicializar un map desde un objeto:

```
let obj = {
  name: "John"
  age: 30
};

let map = new Map (Object.entries(obj));

alert ( map.get('name') ); // John
```

Aqui, Object.entries devuelve el array de pares clave/valor : [ ["name", "John"], ["age", 30] ]. Es lo que necesita Map.

### Object.fromEntries: Objeto desde Map

El metodo Object.fromEntries hace lo contrario: dado un array de pares [ clave,valor ], crea un objeto a partir de ellos:

```
let prices = Object.fromEntries ([
  ['banana', 1],
  ['orange', 2],
  ['meat', 4]
]);

// ahora prices = { banana: 1, orange: 2, meat: 4 }

alert (prices.orange); // 2
```

Podemos usar Object.fromEntries para obtener un objeto desde Map. 

Ejemplo: almacenamos los datos en un map, pero necesitamos pasarlos a un codigo de terceros que espera un objeto simple:

```
let map = new Map();
map.set ('banana', 1);
map.set ('orange', 2);
map.set ('meat', 4);

let obj = Object.fromEntries(map.entries());  // hace un objeto simple (*)

// Hecho!
// obj = { banana:1, orange:2, meat: 4 }

alert (obj.orange); // 2
```

Tambien podriamos acortar la linea (*):

```
let obj = Object.fromEntries(map);  // omitimos .entries()
```

## Set

Un set es un "conjunto de valores" (sin claves), donde cada valor puede aparecer solo una vez.

Sus principales metodos son:

- new Set (iterable) --- crea el set. El arg opcional es un obj iterable (gralmente un array) con valores para inicializarlo.
- set.add (valor) --- agrega un valor, y devuelve el set en si.
- set.delete (valor) --- elimina el valor, y devuelve true si el valor existia al momento de la llamada, sino, devuelve false.
- set.has (valor) --- devuelve true si el valor existe en el set, si no, devuelve false.
- set.clear() --- elimina todo el contenido del set.
- set.size --- es la cantidad de elementos.

La caracteristica principal es que llamadas repetidas de set.add(valor) con el mismo valor no hacen nada. Esa es la razon por la cual cada valor aparece en Set solo una vez.

Por ejemplo, vienen visitantes y queremos recordarlos a todos. Pero las visitas repetidas no deberian llevar a duplicados. Un visitante debe ser "contado" solo una vez. 

Set es lo correcto para eso:

```
let set = new Set();

let john = { name: "John" };
let pete = { name: "Pete" };
let mary = { name: "Mary" };

// visitas, algunos usuarios lo hacen varias veces
set.add(john);
set.add(pete);
set.add(mary);
set.add(john);
set.add(mary);

// set solo guarda valores únicos
alert( set.size ); // 3

for (let user of set) {
  alert(user.name); // John (luego Pete y Mary)
}
```

### Iteracion sobre Set

Podemos recorrer Set con for..of o usando forEach:

```
let set = new Set (["oranges","apples","bananas"]);

for (let value of set) alert (value);

// lo mismo que forEach:

set.forEach ((value, valueAgain, set) => {
  alert(value);
});
```

Tambien soporta los mismos metodos que Map tiene para los iteradores:

- set.keys () --- devuelve un iterable para las claves.
- set.values () --- lo mismo que set.keys (), por su compatibilidad con Map.
- set.entries () --- devuelve un iterable para las entradas [clave, valor], por su compatibilidad con Map.


## WeakMap y Weakset

La primera diferencia con Map es que las claves WeakMap deben ser objetos, no valores primitivos: 

```
let weakMap = new WeakMap();

let obj ={};

weakMap.set (obj, "ok"); // funciona bien (la clave es un objeto)

// no se puede usar un string como clave
weakMap.set ("test","Whoops"); // Error, porque "test" no es un objeto
```

Ahora, si usamos un objeto como clave y no hay otras referencias a ese objeto, se eliminara de la memoria (y del map) automaticamente.

```
let john = { name: "John"};

let weakMap = new WeakMap();
weakMap.set(John, "...");

john = null; // sobreescribe la referencia

// John se elimino de la memoria!
```

Comparado con el ejemplo del Map anterior, ahora si john solo existe como la clave de WeakMap, se eliminara automaticamente del map (y de la memoria).

WeakMap no admite la iteracion ni los metodos keys(), values(), entries(), asi que no hay forma de obtener todas las claves o valores de el.

WeakMap tiene solo los siguientes metodos:

- weakMap.get (clave)
- weakMap.set (clave, valor)
- weakMap.delete (clave)
- weakMap.has (clave)

WeakSet es una coleccion tipo Set que almacena solo objetos y los elimina una vez que se vuelven inaccesibles por otros medios.
WeakMap y WeakSet se utilizan como estructuras de dato "secundarias" ademas del almacenamiento de objetos "principal". Una vez que el objeto se elimina del almacenamiento principal, si solo se encuentra como la clave de WeakMap o en un WeakSet, se limpiara automaticamente.

## Object.keys, values, entries

Para objetos simples, los siguientes metodos estan disponibles:

- Object.keys(obj) --- devuelve un array de propiedades
- Object.values(obj) --- devuelve un array de valores
- Object.entries(obj) --- devuelve un array de pares [propiedad, valor].

Diferencias con map

map.keys() devuelve un iterable. Object.keys(obj) devuelve un Array "real" (y no es obj.keys() ). Por ejemplo:

```
let user = {
  name:"John",
  age: 30
};
```

- Object.keys(user) = ["name","age"]
- Object.values(user) = ["John", 30]
- Object.entries(user) = [ ["name","John"] , [ "age",30] ]

Ejemplo del uso de Object.values para recorrer los valores de propiedad:

```
let user = {
  name:"John",
  age: 30
};

// bucle sobre los valores

for(let value of Object.values(user)){
  alert(value); // John, luego 30
}
```

### Transformando objetos

Los objetos carecen de muchos metodos que existen para los arrays, tales como map, filter y otros.
Si queremos aplicarlos, entonces podemos usar Object.entries seguido de Object.fromEntries:

1. Use Object.entries(obj) para obtener un array de pares clave/valor de obj.
2. Use metodos de array en ese array, por ejemplo map para transformar estos pares clave/valor.
3. Use Object.fromEntries(array) en el array resultante para convertirlo nuevamente en un objeto.

Por ejemplo, tenemos un objeto con precios y queremos duplicarlos:

```
let prices = {
  banana: 1,
  naranja: 2,
  carne: 4,
};

let doublePrices = Object.fromEntries(
  // convertir precios a array, map - cada par clave/valor en otro par
  // y luego fromEntries nos devuelve el objeto
  Object.entries(prices).map(([key, value])) => [key, value * 2]
);

alert (doublePrices.carne); // 8
```


## Asignacion desestructurante

Es una sintaxis especial que nos permite "desempaquetar" arrays u objetos en varias variables, porque a veces es mas conveniente.

### Desestructuracion de Arrays

```
let arr = ["John", "Smith"]

//asignacion desestructurante
// fija firstName = arr[0]
// y surname = arr[1]
let [firstName, surname] = arr;

alert (firstName);  // John
alert (surname);  // Smith
```

### Desestructuracion de Objetos

```
let options = {
  title: "Menu",
  width: 100,
  height: 200
};

let {title,width, height} = options;

alert (title);     // Menu
alert (width);     // 100
alert (height);    // 200
```

### Desestructuracion anidada

```
let options = {
  size: {
    width: 100,
    height: 200
  },
  items: ["Cake", "Donut"],
  extra: true
};

// asignacion desestructurante (hecha en varias lineas para mejor claridad)

let{
  size:{
    width,
    height
  },
  items: [item1, item2], // asignar items aqui
  title= "Menu" // no se encuentra en el objeto (se utiliza valor predeterminado)
} = options;

alert (title);   // Menu
alert (width);   // 100
alert (height);  // 200
alert (item1);   // Cake
alert (item2);   // Donut
```

## Fecha y hora

- En JS la fecha y la hora se representan con el objeto *Date*. No es posible obtener solo la fecha o solo la hora: los objetos *Date* incluyen ambas.
- Los meses se cuentan desde el cero (enero es el mes cero).
- Los dias de la semana en *getDay()* tambien se cuentan desde el cero (que corresponde al dia domingo).
- El objeto *Date* se autocorrige cuando recibe un componente fuera de rango. Es util para sumar o restar dias/meses/horas.
- Las fechas se pueden restar entre si, dando el resultado expresado en milisegundos: esto se debe a que el objeto *Date* toma el valor del *timestamps* cuando es convertido a numero.
- Para obtener el *timestamp* actual de manera inmediata se utiliza Date.now().


## Metodos JSON , toJSON

- JSON es un formato de datos que tiene su propio estandar independiente y librerias para la mayoria de los lenguajes de programacion.
- JSON admite objetos simples, arrays, strings, numeros, booleanos y null.
- JS proporciona los metodos JSON.stringify para serializar en JSON y JSON.parse para leer desde JSON.
- Ambos metodos admiten funciones transformadoras para lectura/escritura inteligente.
- Si un objeto tiene toJSON, entonces es llamado por JSON.stringify.