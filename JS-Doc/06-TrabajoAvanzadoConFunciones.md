# Indicadores y descriptores de propiedad

Como ya sabemos, los objetos pueden almacenar propiedades. Hasta ahora, para nosotros una propiedad era un simple par "clave-valor". Pero una propiedad de un objeto es algo mas flexible y poderoso.

## Indicadores de propiedad

Las propiedades de objeto, aparte de un **value**, tienen tres atributos especiales( tambien llamados "indicadores"):

- **writable**: si es **true**, puede ser editado, de otra manera es de solo lectura.
- **enumerable**: si es **true**, puede ser listado en bucles, de otro modo no puede serlo.
- **configurable**: si es **true**, la propiedad puede ser borrada y estos atributos pueden ser modificados, de otra forma no.

Generalmente no se muestran. Cuando creamos una propiedad "de la forma usual", todos ellos son **true**. Pero podemos cambiarlos en cualquier momento.

Primero, veamos como conseguir estos indicadores.

El metodo **Object.getOwnPropertyDescriptor** permite consultar *toda* la informacion sobre una propiedad.

La sintaxis es:

```
let descriptor = Object.getOwnPropertyDescriptor(obj,propertyName);
```

**obj**
El objeto del que se quiere obtener la informacion

**propertyName**
El nombre de la propiedad

El valor devuelto es el objeto llamado "descriptor de propiedad": este contiene el valor de todos los indicadores.
Por ejemplo:

```
let user = {
    name: "Juan"
};

let descriptor = Object.getOwnPropertyDescriptor(user,'name');

alert(JSON.stringify(descriptor,null,2) );
/* descriptor de propiedad:
{
    "value": "Juan",
    "writable: true,
    "enumerable": true,
    "configurable": true
}
*/
```
Para modificar los indicadores podemos usar **Object.defineProperty**.

La sintaxis es:

```
Object.defineProperty(obj, propertyName, descriptor)
```

**obj, propertyName**
el objeto y la propiedad con los que se va a trabajar.

**descriptor**
descriptor de propiedad a aplicar.

Si la propiedad existe, **defineProperty** actualiza sus indicadores. De otra forma, creara la propiedad con el valor y el indicador dado; en ese caso, si el indicador no es proporcionado, es asumido como *false*.

```
let user ={};

Object.defineProperty(user,"name",{
    value:"Juan"
})
```