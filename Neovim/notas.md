# Modos

Se trabaja a través de 4 modos.

## Modo *Normal*

- Permite al usuario navegar por el archivo

## Modo *Insert*

- Permite editar el texto del archivo, añadir nuevo contenido o eliminarlo
- Se accede apretando **i**.

## Modo *Visual*

- Se utiliza para seleccionar texto.
- Se accede presionando **v** desde el modo normal.
- para salir del modo **ESC**.

## Modo *Comando*

- Acciones como buscar en el texto, salir, guardar, borrar lineas y más.
- Se accede a este modo escribiendo **:** seguido del comando a ejecutar.

### Comandos

- <mark>:q</mark> = Para salir, si es que no hay cambios realizados.
- <mark>:q!</mark>= Sale del archivo descartando los cambios sin guardar.
- <mark>:w</mark>= guarda los cambios y se continua editando.
- <mark>:wq</mark>= guarda los cambios y sale de Neovim


# Shortcuts

- **ctrl + n** : panel lateral de archivos
- **space + f + f**: buscador de archivos
