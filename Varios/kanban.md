# Que es Kanban?

La metodología Kanban se implementa por medio de tableros Kanban. Se trata de un método visual de gestión de proyectos que permite a los equipos visualizar sus flujos de trabajo y la carga de trabajo. En un tablero Kanban, el trabajo se muestra en un proyecto en forma de tablero organizado por columnas. Tradicionalmente, cada columna representa una etapa del trabajo. El tablero Kanban más básico puede presentar columnas como Trabajo pendiente, En progreso y Terminado. Las tareas individuales —representadas por tarjetas visuales en el tablero— avanzan a través de las diferentes columnas hasta que estén finalizadas.

## Principios Kanban

1. Empieza con lo que haces ahora
Puedes implementar el marco Kanban a cualquier proceso o flujo de trabajo actual. A diferencia de algunos procesos de gestión ágil más definidos como Scrum, el marco Kanban es lo suficientemente flexible como para adaptarse a las prácticas centrales de tu equipo de trabajo.
2. Comprométete a buscar e implementar cambios progresivos y evolutivos.
Los grandes cambios pueden ser perjudiciales para tu equipo y, si intentas cambiar todo a la vez, es posible que el nuevo sistema no funcione como esperabas. Ese es el motivo por el cual el marco Kanban está diseñado para fomentar la mejora continua y el cambio progresivo. En lugar de cambiar todo de una vez, empieza por buscar cambios progresivos para lograr que los procesos de tu equipo realmente evolucionen con el tiempo.
3. Respeta los procesos, los roles y las responsabilidades actuales
A diferencia de otras metodologías Lean, Kanban no tiene roles integrados y puede funcionar con la estructura y los procesos actuales de tu equipo. Además, tu proceso actual podría tener elementos excelentes, que se perderían si intentaras modificar completamente tu sistema de trabajo de un momento a otro.
4. Impulsa el liderazgo en todos los niveles
Con el espíritu de mejora continua, el método Kanban reconoce que el cambio puede provenir de cualquier dirección y no solo “de arriba abajo”. Con Kanban, se alienta a los miembros del equipo a participar, proponer nuevas formas para lograr que los procesos evolucionen y emprender nuevas iniciativas de trabajo.

## 6 Practicas de la metodologia Kanban

1. Visualizar el trabajo.
Una de las principales ventajas de Kanban es que puedes visualizar cómo el trabajo “avanza” a través de las etapas. Una tarjeta Kanban de tarea comenzará su viaje en el lado izquierdo de tu tablero y, a medida que tu equipo trabaja en ella, recorrerá lentamente las siguientes etapas hasta que aterrice en la columna Finalizadas. Esta práctica no solo te brinda una idea general de cómo el trabajo avanza a través de las etapas, sino que también te permite obtener información en tiempo real y apreciar de un vistazo el estado de los proyectos.
2. Limitar el trabajo en curso.
Como metodología ágil, Kanban se centra en un principio de entrega temprana, lo que implica que las tareas deben moverse rápidamente de una columna a otra en lugar de estancarse en un estado ambiguo de “trabajo en progreso” (wip). Aunque no existe un requisito establecido sobre cuántas tareas deben estar “en progreso” en un momento dado, es importante establecer los límites del trabajo; por lo que anima a tu equipo a centrarse en finalizar tareas individuales y a evitar realizar varias tareas a la vez.
3. Gestionar el flujo de trabajo.
La práctica n.° 2 recomienda limitar la cantidad de trabajo en curso, y la mejor manera de hacerlo es con la optimización del flujo de tareas dentro del tablero Kanban. Gestionar y mejorar el flujo de trabajo te permitirá controlar el tiempo predestinado para el trabajo y así poder reducir el tiempo de entrega (el tiempo que pasa entre el inicio de una tarea hasta que llega a la columna Finalizadas de tu tablero Kanban) y garantizar que estás entregando tareas o enviando nuevos productos mientras siguen siendo relevantes.

4. Implementar políticas de procesos explícitas.
Debido a la rapidez con la que se mueven las tareas en Kanban, asegúrate de que tu equipo haya establecido y comunicado claramente las convenciones. Las políticas de tu proceso deben guiar a tu equipo en la implementación de la metodología Kanban. Además, se debe alentar a todos en el equipo a participar e innovar las políticas Kanban, tal como se establece en el cuarto principio básico de Kanban: Impulsar el liderazgo en todos los niveles.

5. Implementar ciclos de comentarios.
En Kanban, necesitas recopilar comentarios de dos grupos distintos: tus clientes y tu equipo.

Recopila comentarios de tus clientes sobre la calidad y eficacia de la solución que produjo el equipo. ¿Fue el producto adecuado? ¿Hubo algún problema? En el caso de que haya surgido algún problema, como errores en un código o cualquier otro defecto del producto, revisa tu flujo Kanban y agrega más tiempo para la revisión, los ajustes y la evaluación.

Realiza consultas frecuentes con el equipo sobre el proceso de ejecución de un marco Kanban. ¿Cómo se sienten con los resultados? Aquí tienes otra oportunidad para fomentar el liderazgo en todos los niveles y mejorar las políticas de procesos del equipo.

6. Mejorar colaborando y evolucionar experimentando
En esencia, Kanban se trata de una mejora continua. Sin embargo, también significa que otros sistemas podrían funcionar bien junto con Kanban. Ya sea Scrum o alguna otra metodología, debes estar siempre dispuesto a colaborar, experimentar y desarrollar tus procesos si es necesario.

## Como implementar la metodologia Kanban

1. Crea columnas para representar el trabajo.
Tradicionalmente, las columnas del tablero Kanban representan las distintas etapas del trabajo. Aunque las columnas que crees dependerán de tu equipo, te mostramos a continuación los tipos de columnas más comunes:

- Trabajo pendiente, Bandeja de entrada o Nuevo: Esta es la columna donde se agregará el trabajo nuevo antes de ser asignado a un miembro del equipo.

- Listo o Prioritario: Mueve el trabajo a esta columna cuando esté listo para empezar a trabajar.

- En progreso: Se trata de las tareas en las que se está trabajando actualmente. Dependiendo de las necesidades del equipo, puedes desglosar la columna ‘En progreso’ en varias columnas más. Por ejemplo, un equipo de contenido puede crear columnas para Redacción, Revisión y Edición; en tanto un equipo de ingeniería puede crear columnas para Desarrollo, Pruebas e Implementación.

- En espera: Mueve la tarea a esta columna si el trabajo presenta algunos cuellos de botella por algún motivo.

- Finalizadas o Trabajo terminado: Las tareas deberían aparecer en esta columna una vez que hayan sido finalizadas.

2. Agrega tareas para representar el trabajo.
En un tablero Kanban, cada tarea está representada por una tarjeta. Asegúrate de que los títulos de las tareas sean concretos. Recomendamos empezar con un verbo para que tu equipo sepa exactamente qué debe hacer.

Si usas una solución virtual de gestión del trabajo, también puedes agregar información adicional, contexto y archivos a las tarjetas Kanban. Luego, usa las etiquetas para dar seguimiento a los metadatos, como la duración o la priorización de la tarea.

3. Haz que el trabajo avance por las etapas.
Un elemento central de la gestión del flujo de trabajo con los tableros Kanban es hacer avanzar el trabajo por las diferentes etapas. Puedes hacerlo manualmente arrastrando y soltando tareas, o puedes buscar una solución virtual de gestión del trabajo que te permita automatizar este proceso. Por ejemplo, con Asana puedes establecer reglas para orientar automáticamente los elementos de trabajo a las diferentes columnas según la información relevante de la tarea. Por ejemplo, puedes establecer una regla para mover una tarea a la columna ‘En progreso’ una vez que se haya asignado a un miembro del equipo.
