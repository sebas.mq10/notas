# **Documentacion de procesos** 

1. Documentacion de configuracion de entorno detallado paso por paso con explicacion y posibles errores (con su solucion)
2. Documentacion de configuracion de Visual Studio Code (prettier, linter,etc)
3. Documentacion de creacion y clonacion de repositorios
4. Documentacion de errores comunes de git y gitlab
5. Documentacion de errores comunes con tdd
