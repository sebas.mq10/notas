# Docker Engine y Docker Desktop

1. Docker Engine:

Docker Engine es el componente central de Docker y está diseñado para funcionar en varios sistemas operativos, incluidos Linux, Windows y macOS. Proporciona la funcionalidad esencial para la gestión de contenedores, incluyendo la creación, arranque, parada y eliminación de contenedores. Docker Engine permite ejecutar y gestionar contenedores directamente desde la línea de comandos.

2. Docker Desktop:

Docker Desktop es una aplicación que proporciona una interfaz fácil de usar para gestionar Docker en sistemas operativos macOS y Windows. Incluye Docker Engine, por lo que no es necesario instalarlo por separado. Docker Desktop también viene con características adicionales como una interfaz gráfica de usuario (GUI), un panel de control fácil de usar para la gestión de contenedores y la integración con otras herramientas de desarrollo.

# Instalacion de Docker Engine en Linux

Para instalar Docker Engine necesitas chequear si tu version de Ubuntu es de 64 bits. Para hacerlo correr en la terminal:

```
uname -i
```

Debe devolver **x86_64** lo cual es compatible (al igual que amd64).

## Desinstalar versiones antiguas de Docker Engine

Antes de instalar hay que asegurarse de desinstalar cualquier paquete antiguo que pueda generar conflictos

1. Desinstalar Docker Engine, CLI, containerd y Docker Compose packages:

```
sudo apt-get purge docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin docker-ce-rootless-extras
```

2. Imágenes, containers, volúmenes o archivos de configuración personalizados no se borran automáticamente. Para eliminar todas las imágenes,containers y volúmenes:

```
sudo rm -rf /var/lib/docker
sudo rm -rf /var/lib/containerd
```

https://docs.docker.com/engine/install/ubuntu/#uninstall-docker-engine

Además, Docker Engine depende de containerd y runc. Docker Engine agrupa estas dependencias en un único paquete: containerd.io. Si ha instalado containerd o runc anteriormente, desinstálelos para evitar conflictos con las versiones incluidas en Docker Engine.

Correr el siguiente comando para desinstalar todos los paquetes con conflictos:

```
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done
```

## Instalar usando el repositorio apt

### Setupear el repositorio 

1. Actualizar el indice de paquetes apt e instalar paquetes para permitir que apt use un repositorio a través de HTTPS:

```
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
```
2. Agregar la clave GPG oficial de Docker:

```
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
```
3. Usar el siguiente comando para setupear el repositorio:

```
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

### Instalar Docker Engine 

1. Actualizar el indice del paquete apt:

```
sudo apt-get update
```
2. Instalar Docker Engine, containerd y Docker Compose 

Para instalar la última versión, correr:

```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```
3. Verificar que la instalacion de Docker Engine fue exitosa corriendo la imagen **hello-world**

```
sudo docker run hello-world
```

# Instalar Docker Desktop en Linux

Enfoque recomendado para instalar Docker Desktop en Ubuntu:

1. Configurar el repositorio de paquetes de Docker.
Antes de instalar Docker Engine por primera vez en una nueva máquina host, debe configurar el repositorio de Docker. Luego, puede instalar y actualizar Docker desde el repositorio.

- Actualizar el indice de paquetes apt e instalar paquetes para permitir que apt use un repositorio a través de HTTPS:

```
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
```

- Agregar la clave GPG oficial de Docker:

```
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg
```

- Usar el siguiente comando para configurar el repositorio:

```
echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

2. Descargar el último paquete DEB ("https://desktop.docker.com/linux/main/amd64/docker-desktop-4.21.1-amd64.deb?utm_source=docker&utm_medium=webreferral&utm_campaign=docs-driven-download-linux-amd64&_gl=1*1n7p0ma*_ga*MTExOTk0MDc1NS4xNjg3MzU4MDE1*_ga_XJWPQMJYHQ*MTY4OTk1NjQzOC45LjEuMTY4OTk1Njk4MS42MC4wLjA.")

3. Instalar el paquete con apt de la siguiente manera:

Ir a la carpeta donde se descargó el archivo (Downloads) y correr los siguientes comandos:

```
sudo apt-get update 
sudo apt-get install ./docker-desktop-4.21.1-amd64.deb
```

### Abrir Docker Desktop 

```
systemctl --user start docker-desktop
```

## Comandos Comunes (usando el ejemplo de la documentación - "getting-started")

- Construir la imágen del contenedor

```
sudo docker build -t getting-started .
```

- Iniciar el contenedor 

```
sudo docker run -dp 127.0.0.1:3000:3000 getting-started
```
- Ejemplo para iniciar un container y crear un archivo con un número random entre 1 y 10000

```
sudo docker run -d ubuntu bash -c "shuf -i 1-10000 -n -o /data.txt && tail -f /dev/null"
```

- Ver lista de containers corriendo y su ID 

```
sudo docker ps
```

- Ver lista de imágenes

```
sudo docker image ls
```

- Ver logs

```
sudo docker logs <container-id>
```

- Acceder a un container y ver su salida

```
sudo docker exec <container-id> cat /data.txt
```

- Detener un contenedor (reemplazar <the-container-id> con el ID que vemos con docker ps)

```
sudo docker stop <the-container-id>
```

- Remover un contenedor una vez que lo detuvimos

```
sudo docker rm <the-container-id>
```

- Crear un volumen e inicializar el container 

```
sudo docker volume create <todo-db>
```

```
sudo docker run -dp 127.0.0.1:3000:3000 --mount type=volume,src=todo-db,target=/etc/todos getting-started
```

- Ver la locación donde se guarda la data cuando usas un volumen 

```
sudo docker volume inspect <todo-db>
```

Mountpoint es la locación actual de la data.
