# Generacion de SSH Key 

1. En la terminal escribir: ssh-keygen -t ed25519
2. Luego debemos buscar la clave poniendo el siguiente comando:
    cat ~/.ssh/id_ed25519.pub
3. Luego debemos copiar la clave que nos proporciono en gitlab
    Vamos a gitlab - preferences - SSH keys y copiamos la key en el campo. Guardamos y listo.
