# App de muestra con Minikube

Este tutorial muestra como ejecutar una app de muestra en Kubernetes usando Minikube de manera local. Primero debemos instalar Minikube,Kubectl y Kubeadm (y tener Docker también).

Primero podemos chequear si tenemos o no instalado lo necesario:

`
docker --version
kubectl version --client
minikube version
`

### Instalar el binario Kubectl con curl

`
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
`

### Instalar Kubectl (linea de comando para comunicarse con el clúster)

`
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
`

- Para asegurarse de que la version instalada esta actualizada a la fecha:

`
kubectl version --client 
`

- Para ver la versión mas detalladamente:

`
kubectl version --client --output=yaml
`

### Instalar Kubeadm (comando para arrancar el cluster) y Kubelet (inicia pods y contenedores)

1. Actualice el índice del paquete apt e instale los paquetes necesarios para usar el repositorio apt de Kubernetes:

```
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl
```

2. Descargue la clave de firma pública para los repositorios de paquetes de Kubernetes. Se utiliza la misma clave de firma para todos los repositorios, por lo que puede ignorar la versión en la URL:

`
curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
`

3. Agregue el repositorio apt de Kubernetes apropiado:

`
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
`

4. Actualice el índice del paquete apt, instale kubelet y kubeadm:

```
sudo apt-get update
sudo apt-get install -y kubelet kubeadm
sudo apt-mark hold kubelet kubeadm
```

### Instalar Minikube

```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
```

Arrancar el clúster 

`
minikube start
`

Habilitar addon de entrada para administrar el acceso externo a los servicios que se ejecutan en el clúster

`
minikube addons enable ingress
`

Creamos un directorio donde guardaremos los archivos YAML

`
mkdir k8s-manifests
`


Crear una implementación de Kubernetes simple que ejecute "Hello World". Crear un archivo YAML (hello-world-deployment.yaml) con lo siguiente:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: hello-world-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: hello-world
  template:
    metadata:
      labels:
        app: hello-world
    spec:
      containers:
      - name: hello-world
        image: nginx:latest
        ports:
        - containerPort: 80
```

Aplicar la implementacion en el cluster minikube:

`
kubectl apply -f hello-world-deployment.yaml
`

Crear un servicio de Kubernetes. (hello-world-service.yaml)

```
apiVersion: v1
kind: Service
metadata:
  name: hello-world-service
spec:
  selector:
    app: hello-world
  ports:
  - protocol: TCP
    port: 80
    targetPort: 80
  type: NodePort
```

Aplicar el servicio

`
kubectl apply -f hello-world-service.yaml
`

Configurar el ingreso para acceder al servicio "Hello World" desde el navegador, crear un manifiesto YAML de ingress (hello-world-ingress.yaml):

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: hello-world-ingress
spec:
  rules:
  - host: hello-world.local
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: hello-world-service
            port:
              number: 80
```

Aplicar el servicio:

`
kubectl apply -f hello-world-ingress.yaml
`

Actualizar archivo de hosts (Linux/macOS):
Tenemos que editar el archivo de hosts de su sistema para asignar el nombre de host de Ingress a la dirección IP de Minikube. Primero nos fijamos cual es la ip corriendo:

`
minikube ip
`

Luego abrimos el archivo de hosts en un editor de texto (nano, neovim, etc) con privilegios de root:

`
sudo nano /etc/hosts
`

Y agregamos la siguiente linea:

```
<minikube-ip> hello-world.local
```

### Configuración de Nginx 

Crear una imágen de Docker personalizada con una configuración modificada que muestre el "Hello World". Creamos un archivo Dockerfile.

`
touch Dockerfile
`

Agregamos las siguientes lineas en el archivo Dockerfile:

```
FROM nginx:latest
COPY hello-world.html /usr/share/nginx/html/index.html
```

Creamos el archivo HTML "Hello World":

```
<!DOCTYPE html>
<html>
<head>
    <title>Hello World</title>
</head>
<body>
    <h1>Hello, World!</h1>
</body>
</html>
```

Crear la imágen de Docker personalizada:

`
docker build -t my-nginx-image .
`

Etiquetamos la imágen con tu nombre de usuario de Docker Hub y un nombre de repositorio (en mi caso sebasmq10 y "latest"). 

`
docker tag sebasmq10/my-nginx-image:latest
`

Nos logueamos en docker

`
docker login
`

Y pusheamos

`
docker push sebasmq10/my-nginx-image:latest
`

Updateamos el YAML de implementación:

```
spec:
  containers:
  - name: hello-world
    image: sebasmq10/my-nginx-image:latest
    ports:
    - containerPort: 80
```

Y aplicamos

`
kubectl apply -f hello-world-deployment.yaml
`

Chequeamos el status

`
kubectl get deployment hello-world-deployment
kubectl get pods -l app=hello-world
`

Podemos monitorear el estado de implementación usando:

`
kubectl rollout status deployment hello-world-deployment
`

## Accedemos a la aplicación

Una vez que los pods se hayan implementado correctamente, accedemos a http://hello-world.local en el browser.
