# Multipass - Herramienta para crear maquinas virtuales.

https://multipass.run/install

## Instalar Multipass en linux

```
sudo snap install multipass
```

## Crear instancias Multipass

```
multipass launch -n master -c 2 -m 4G -d 20G
```
-n master: esta linea es para darle nombre a la máquina virtual (Por convención habrá uno llamado master,que será el control plane, y otros dos llamados "worker1" y "worker2"). 
-c 2: se definen dos núcleos para la misma.
-m 4G: se define que la misma tendrá disponible 4GB de ram.
-d 20G: se define que tendrá 20GB de disco.

Se repite dos veces mas, con los nombres "worker1" y "worker2" por ejemplo.

## Comandos Multipass


Ver la lista de VM (máquinas virtuales):

```
multipass list
```

Startear las VM:

```
multipass start master worker1 worker2
```

Parar las VM:

```
multipass stop master worker1 worker2
```

Abrir la VM en la terminal:

```
multipass shell master
```

(Lo hacemos con las 3 VM, "multipass shell worker1, etc")


## Instalaciones en todas las VM


https://www.youtube.com/watch?v=_WW16Sp8-Jw

```
sudo apt-get update
```
- Deshabilitar el swap

```
sudo swapoff -a
```

No producirá ninguna salida.

- Desactivar el firewall:

```
sudo ufw disable
```

Devolverá la siguiente salida:

```
Firewall stopped and disabled on system startup
```

- Instalar docker:

```
sudo apt install docker.io -y
```

- Habilitar y startear docker 

```
sudo systemctl enable docker
```

```
sudo systemctl start docker
```

Para ver el estado de docker:

```
sudo systemctl status docker
```

- Instalar Curl

```
sudo apt install apt-transport-https curl -y
```

- Añadir la key del repositorio para descargar e instalar Kubernetes

```
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
```

- Añadir el repositorio actual

```
sudo bash -c 'echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list'
```

- Instalar kubernetes 

```
sudo apt-get update && sudo apt-get install -y kubelet kubeadm kubectl
```

## Construir el cluster de Kubernetes 

- Solo en el control plane (master):

```
sudo kubeadm init --pod-network-cidr={ip} --ignore-preflight-errors=all
```

Nos devolverá algo parecido a esto:

```
kubeadm join 10.85.250.75:6443 --token 1ujt3j.j5f1kmdstwugmftd \
        --discovery-token-ca-cert-hash sha256:4a59b551cba608f3e9f7598ad758105a8ecc7e70e827224a8a2434d2ca9c1843
```

Luego en los nodos worker1 y worker2 pegaremos ese último comando (agregando sudo adelante).

En Master ingresamos los comandos que nos informó cuando hicimos el init:


- Primero crearemos el directorio.

```
mkdir -p $HOME/.kube
```
- Acto seguido vamos a configurar los archivos de configuración.

```
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
```

- Por último vamos a cambiar los permisos de dicho directorio y con eso ya tendremos configurado el entorno de usuario.

```
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

Ingresamos como root (sudo -i) y corremos el siguiente comando:

```
export KUBECONFIG=/etc/kubernetes/admin.conf
```

Corremos el siguiente comando para ver el status de los nodos:

```
sudo kubectl get nodes
```

Debemos ver los tres nodos con un status de "NotReady". Si sale un error de conexión entramos como usuario root (sudo -i) y tipeamos lo siguiente:

```
systemctl start kubelet.service
```
